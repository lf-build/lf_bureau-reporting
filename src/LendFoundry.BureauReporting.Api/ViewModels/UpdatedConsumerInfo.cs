﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes the L1 segment of Metro2
    /// </summary>
    public class UpdatedConsumerInfo
    {
        public string ChangeIndicator { get; set; }

        public string NewConsumerAccountNumber { get; set; }

        public string NewIdentificationNumber { get; set; }
    }
}

//UpdatedBorrowerData_ChangeIndicator
//UpdatedBorrowerData_NewAccountNumber
//UpdatedBorrowerData_NewIdentificationNumber

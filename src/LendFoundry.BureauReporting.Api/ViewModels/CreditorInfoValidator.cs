﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class CreditorInfoValidator : ViewModelValidator<CreditorInfo>
    {
        private IValidationContext Context { get; set; }
        public CreditorInfoValidator(CreditorInfo creditorInfo, IValidationContext context) : base(creditorInfo, context)
        {
            Context = context;

            RuleFor(data => data.CreditorClassification)
                                .NotEmpty()
                                    .WithMessage("CreditorClassification is required.")
                                .Must(BeValidClassificationCode)
                                    .WithMessage("CreditorClassificationCode has an invalid value.");

            RuleFor(data => data.OriginalCreditorName)
                                .NotEmpty()
                                .WithMessage("OriginalCreditorName is required.");


            // this does not get reported to credit bureaus. see documentaion. So, should we validate?
            RuleFor(data => data.CreditorAccountNumber)
                                .NotEmpty()
                                .WithMessage("CreditorAccountNumber is required.");

        }

        private bool BeValidClassificationCode(string classificationCode)
        {
            return Context.Lookups.CreditorClassificationCodes.ContainsKey(classificationCode);
        }        
    }
}

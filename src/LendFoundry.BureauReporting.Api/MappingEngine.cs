﻿using AutoMapper;
using LendFoundry.BureauReporting.Api.Models;
using LendFoundry.BureauReporting.Api.Models.Account;
using LendFoundry.BureauReporting.Api.Models.Borrower;
using LendFoundry.BureauReporting.Api.Models.Common;
using LendFoundry.BureauReporting.Api.Models.Loan;
using LendFoundry.BureauReporting.Api.Models.Payments;
using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Account;
using LendFoundry.BureauReporting.Core.Domain.Borrower;
using LendFoundry.BureauReporting.Core.Domain.Common;
using LendFoundry.BureauReporting.Core.Domain.Loan;
using LendFoundry.BureauReporting.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api
{
    public class MappingEngine : IMappingEngine
    {
        public void CreateMappings()
        {

            Mapper.CreateMap<ReportDataModel, ReportData>();
            Mapper.CreateMap<AccountInfoModel, AccountInfo>();
            Mapper.CreateMap<AccountStatusModel, AccountStatus>();
            Mapper.CreateMap<GeneralAccountStatusModel, GeneralAccountStatus>();
            Mapper.CreateMap<ChargeOffStatusModel, ChargeOffStatus>();
            Mapper.CreateMap<ClosureStatusModel, ClosureStatus>();
            Mapper.CreateMap<DelinquencyStatusModel, DelinquencyStatus>();
            Mapper.CreateMap<DisputeStatusModel, DisputeStatus>();
            Mapper.CreateMap<SaleStatusModel, SaleStatus>();
            Mapper.CreateMap<SettledStatusModel, SettledStatus>();

            Mapper.CreateMap<BorrowerInfoModel, BorrowerInfo>();
            Mapper.CreateMap<BankruptcyStatusModel, BankruptcyStatus>();
            Mapper.CreateMap<BorrowerStatusModel, BorrowerStatus>();
            Mapper.CreateMap<GeneralBorrowerStatusModel, GeneralBorrowerStatus>();
            Mapper.CreateMap<TraceabilityModel, Traceability>();

            Mapper.CreateMap<LoanInfoModel, LoanInfo>();
            Mapper.CreateMap<ProductInfoModel, ProductInfo>();
            Mapper.CreateMap<PersonModel, Person>();
            Mapper.CreateMap<AddressModel, Address>();
            Mapper.CreateMap<PhoneModel, Phone>();

            Mapper.CreateMap<PaymentInfoModel, PaymentInfo>();
            Mapper.CreateMap<PaymentRecordModel, PaymentRecord>();
            Mapper.CreateMap<PaymentScheduleModel, PaymentSchedule>();
            Mapper.CreateMap<AmountModel, Amount>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Account
{
    public class SaleStatusModel : AccountStatusModel
    {
        public string SoldTo { get; set; }
        public DateTime SaleDate { get; set; }
    }
}

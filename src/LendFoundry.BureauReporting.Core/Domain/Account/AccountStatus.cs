﻿using LendFoundry.BureauReporting.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class AccountStatus
    {
        public Amount TotalAmountDue { get; set; }
    }
}

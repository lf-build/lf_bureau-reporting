﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using LendFoundry.BureauReporting.Core.Domain.Borrower;
using LendFoundry.BureauReporting.Core.Domain.Loan;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using LendFoundry.BureauReporting.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Abstractions
{
    public interface IReportDataVisitor
    {
        void Visit(AccountInfo accountInfo);
        void Visit(BorrowerInfo borrowerInfo);
        void Visit(LoanInfo loanInfo);
        void Visit(PaymentInfo paymentInfo);
        Metro2Data GetMetro2Data();
    }
}

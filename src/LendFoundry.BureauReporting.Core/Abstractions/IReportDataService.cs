﻿using LendFoundry.BureauReporting.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Abstractions
{
    public interface IReportDataService
    {
        void AddBureauReportData(ICollection<ReportData> reportData);
        void ImportFileData();
        void SubmitReport();
    }
}

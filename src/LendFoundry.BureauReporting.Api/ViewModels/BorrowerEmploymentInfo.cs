﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes the N1 segment of Metro2
    /// </summary>
    public class BorrowerEmploymentInfo
    {
        public Contact EmployerContact { get; set; }

        public string EmployerOccupation { get; set; }
    }
}

//Employer_FirstName
//Employer_Surname
//Employer_MiddleName
//Employer_Generation
//Employer_DateOfBirth
//Employer_SocialSecurityNumber
//Employer_AddressFirstLine
//Employer_AddressIndicator
//Employer_AddressSecondLine
//Employer_CountryCode
//Employer_State
//Employer_City
//Employer_ZipCode
//Employer_TelephoneNumber
//Employer_ResidenceCode
//Employer_Occcupation
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Payments
{
    public class PaymentSchedule
    {
        public DateTime ScheduledDate { get; set; }
        public Amount ScheduledAmountDue { get; set; }
        public Amount OverdueAmount { get; set; }
    }
}

﻿using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class MortgageInfoValidator : ViewModelValidator<MortgageInfo>
    {
        public MortgageInfoValidator(MortgageInfo mortgageInfo, IValidationContext context) : base(mortgageInfo, context)
        {
            // TODO: Not sure if we need to make this mandatory
            //RuleFor(data => data.MortgageIdentificationNumber).NotEmpty();
        }
    }
}

﻿using LendFoundry.BureauReporting.Core.Abstractions;
using LendFoundry.BureauReporting.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Borrower
{
    public class BorrowerInfo
    {
        public Person Borrower { get; set; }
        public Address Address { get; set; }
        public BorrowerStatus Status { get; set; }
        public string PositionInLoan { get; set; }
        public string Role { get; set; } // primary, co-signer etc, maker, co-maker, guarantor etc.
    }
}

﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class ClosureStatus : AccountStatus
    {
        public string Status { get; set; }
        public DateTime ClosureDate { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}

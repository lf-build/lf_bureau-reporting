﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Metro2
{
    public class K4Segment : Segment
    {
        public string SegmentIdentifier { get; set; }

        public string SpecializedPaymentIndicator { get; set; }

        public DateTime? DeferredPaymentStartDate { get; set; }

        public DateTime? PaymentDueDate { get; set; }

        public double? PaymentAmount { get; set; }

        public string Reserved { get; set; }

        public string Accept(IMetro2Visitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}

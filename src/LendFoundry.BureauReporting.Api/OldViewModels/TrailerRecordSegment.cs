﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class TrailerRecordSegment : FileSegment
    {
        public string RecordDescriptorWord { get; set; }
        public string RecordIdentifier { get; set; }
        public int? TotalBaseRecords { get; set; }
        public string Reserved4thPosition { get; set; }
        public string Reserved5thPosition { get; set; }
        public int? TotalAssociatedConsumerSegmentsJ1 { get; set; }
        public int? TotalAssociatedConsumerSegmentsJ2 { get; set; }
        public int? BlockCount { get; set; }
        public string TotalofStatusCodeDA { get; set; }
        public string TotalofStatusCode05 { get; set; }
        public string TotalofStatusCode11 { get; set; }
        public string TotalofStatusCode13 { get; set; }
        public string TotalofStatusCode61 { get; set; }
        public string TotalofStatusCode62 { get; set; }

        public string TotalofStatusCode63 { get; set; }

        public string TotalofStatusCode64 { get; set; }

        public string TotalofStatusCode65 { get; set; }

        public string TotalofStatusCode71 { get; set; }

        public string TotalofStatusCode78 { get; set; }

        public string TotalofStatusCode80 { get; set; }

        public string TotalofStatusCode82 { get; set; }

        public string TotalofStatusCode83 { get; set; }

        public string TotalofStatusCode84 { get; set; }

        public string TotalofStatusCode88 { get; set; }

        public string TotalofStatusCode89 { get; set; }

        public string TotalofStatusCode93 { get; set; }

        public string TotalofStatusCode94 { get; set; }

        public string TotalofStatusCode95 { get; set; }

        public string TotalofStatusCode96 { get; set; }

        public string TotalofStatusCode97 { get; set; }

        public string TotalOfECOACodeZAllSegments { get; set; } //(All Segments)

        public string TotalEmploymentSegments { get; set; }

        public string TotalOriginalCreditorSegments { get; set; }

        public string TotalPurchasedPortfolioOrSoldTo { get; set; }

        public string TotalMortgageInformationSegments { get; set; }

        public string TotalSpecializedPaymentInformationSegments { get; set; }

        public string TotalChangeSegments { get; set; }

        public string TotalSocialSecurityNumbersAllSegments { get; set; } // (All Segments)

        public string TotalSocialSecurityNumbersBaseSegment { get; set; }// (Base Segments)

        public string TotalSocialSecurityNumbersJ1 { get; set; }

        public string TotalSocialSecurityNumbersJ2 { get; set; }

        public string TotalDatesofBirthAllSegments { get; set; } //(All Segments)

        public string TotalDatesofBirthBaseSegment { get; set; }


        public string TotalDatesOfBirthJ1 { get; set; }

        public string TotalDatesOfBirthJ2 { get; set; }

        public string TotalTelephoneNumbersAllSegments { get; set; }

        public string Reserved47thPosition { get; set; }
    }
}
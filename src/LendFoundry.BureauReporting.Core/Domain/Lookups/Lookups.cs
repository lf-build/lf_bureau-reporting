﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Lookups
{
    public class Lookups : ILookups
    {
        #region Methods
        private Tuple<string, bool, bool, bool, bool, bool> CreateTypeRecord(string accountTypeCode, bool isLineOfCredit, bool isInstallment, bool isMortgage, bool isOpen, bool isRevolving)
        {
            return new Tuple<string, bool, bool, bool, bool, bool>(accountTypeCode, isLineOfCredit, isInstallment, isMortgage, isOpen, isRevolving);
        }
        #endregion Methods

        #region Account Status Lookup
        public Dictionary<string, string> AccountStatusCodes => new Dictionary<string, string>
        {
            { "AccountTransferred", "05" },
            { "Current", "11" },
            { "Paid", "13" },
            { "Closed", "13" },
            { "ZeroBalance", "13" },
            { "VoluntarySurrenderPaidInFull", "61" },
            { "CollectionAccountPaidInFull", "62" },
            { "RepossessedPaidInFull", "63" },
            { "ChargeOffPaidInFull", "64" },
            { "ForeclosureStartedPaidInFull", "65" },
            { "30To59DaysPastDueDate", "71" },
            { "60To89DaysPastDueDate", "78" },
            { "90To119DaysPastDueDate", "80" },
            { "120To149DaysPastDueDate", "82" },
            { "150To179DaysPastDueDate", "83" },
            { "MoreThan180DaysPastDueDate", "84" },
            { "InsuranceClaimFiled", "88" },
            { "DeedReceivedForForeclosure", "89" },
            { "AssignedToCollections", "93" },
            { "ForeclosureCompleted", "94" },
            { "VoluntarySurrenderBalanceDue", "95" },
            { "MerchandiseRepossedBalanceDue", "96" },
            { "LossDueToUnpaidBalance", "97" },
            { "DeleteAccountNoFraud", "DA" },
            { "DeleteAccountConfirmedFraud", "DF" }
        };

        #endregion Account Status Lookup

        #region Account Type Lookup
        public Dictionary<string, string> AccountTypes => new Dictionary<string, string>
        {
            { "Agricultural", "7B" },
            { "AttorneyFees", "95" },
            { "Auto", "00" },
            { "AutoLease", "3A" },
            { "BusinessCreditCard", "8A" },
            { "BusinessLinePersonallyGuaranteed", "9B" },
            { "BusinessLoan", "10" },
            { "ChargeAccount", "07" },
            { "ChildSupport", "93" },
            { "CollectionAgency", "48" },
            { "CombinedCreditPlan", "37" },
            { "CommercialInstallmentLoan", "6A" },
            { "CommercialLineOfCredit", "7A" },
            { "CommercialMortgageLoan", "6B" },
            { "ConstructionLoan", "0F" },
            { "ConventionalRealEstateMortgage", "26" },
            { "CreditCard", "18" },
            { "CreditLineSecured", "47" },
            { "DebitCard", "43" },
            { "DebtBuyer", "0C" },
            { "DebtConsolidation", "91" },
            { "DepositAccountWithOverdraftProtection", "8B" },
            { "Education", "12" },
            { "FamilySupport", "50" },
            { "FHAHomeImprovementLoan", "05" },
            { "FHARealEstateMortgageLoan", "19" },
            { "FlexibleSpendingCreditCard", "0G" },
            { "GovtBenefit", "75" },
            { "GovtEmployeeAdvance", "73" },
            { "GovtFeeForServices", "72" },
            { "GovtFine", "71" },
            { "GovtGrant", "69" },
            { "GovtMiscDebt", "74" },
            { "GovtOverpayment", "70" },
            { "GovtSecuredDirectLoan", "78" },
            { "GovtSecuredGuaranteedLoan", "66" },
            { "GovtUnsecuredDirectLoan", "67" },
            { "GovtUnsecuredGuaranteedLoan", "65" },
            { "HomeEquity", "6D" },
            { "HomeEquityLineOfCredit", "89" },
            { "HomeImprovement", "04" },
            { "InstallmentSalesContract", "06" },
            { "Lease", "13" },
            { "LineOfCredit", "15" },
            { "ManufacturedHousing", "17" },
            { "MedicalDebt", "90" },
            { "NoteLoan", "20" },
            { "PartiallySecured", "03" },
            { "RealEstateJrLiensNonPurchaseMoneyFirst", "5A" },
            { "RealEstateSpecificTypeUnknown", "08" },
            { "RecreationalMerchandise", "11" },
            { "RentalAgreement", "29" },
            { "ReturnedCheck", "77" },
            { "SecondMortgage", "5B" },
            { "Secured", "02" },
            { "SecuredCreditCard", "2A" },
            { "SecuredHomeImprovement", "9A" },
            { "Telecommunications", "4D" },
            { "TimeShareLoan", "0A" },
            { "Unsecured", "01" },
            { "USDARealEstateMortgageLoan", "2C" },
            { "UtilityCompany", "92" },
            { "VetsAdminRealEstateMortgageLoan", "25" }
        };

        #endregion Account Type Lookup

        #region Account Type To Portfolio Mapping
        public Dictionary<string, Tuple<string, bool, bool, bool, bool, bool>> AccountTypePortfolioMap => new Dictionary<string, Tuple<string, bool, bool, bool, bool, bool>>
        {
            // AccountType--------------------------------------------------Code------LineOfCredit-------Installment-------Mortgage-------Open-------Revolving
            { "Agricultural",                             CreateTypeRecord( "7B",     false,             true,             false,         false,     false ) },
            { "AttorneyFees",                             CreateTypeRecord( "95",     false,             true,             false,         false,     false ) },
            { "Auto",                                     CreateTypeRecord( "00",     false,             true,             false,         false,     false ) },
            { "AutoLease",                                CreateTypeRecord( "3A",     false,             true,             false,         false,     false ) },
            { "BusinessCreditCard",                       CreateTypeRecord( "8A",     false,             false,            false,         true,      true  ) },
            { "BusinessLinePersonallyGuaranteed",         CreateTypeRecord( "9B",     true,              false,            false,         false,     false ) },
            { "BusinessLoan",                             CreateTypeRecord( "10",     false,             true,             false,         false,     false ) },
            { "ChargeAccount",                            CreateTypeRecord( "07",     false,             false,            false,         false,     true  ) },
            { "ChildSupport",                             CreateTypeRecord( "93",     false,             false,            false,         true,      false ) },
            { "CollectionAgency",                         CreateTypeRecord( "48",     false,             false,            false,         true,      false ) },
            { "CombinedCreditPlan",                       CreateTypeRecord( "37",     false,             false,            false,         true,      true  ) },
            { "CommercialInstallmentLoan",                CreateTypeRecord( "6A",     false,             true,             false,         false,     false ) },
            { "CommercialLineOfCredit",                   CreateTypeRecord( "7A",     true,              false,            false,         false,     false ) },
            { "CommercialMortgageLoan",                   CreateTypeRecord( "6B",     false,             false,            true,          false,     false ) },
            { "ConstructionLoan",                         CreateTypeRecord( "0F",     false,             true,             false,         false,     false ) },
            { "ConventionalRealEstateMortgage",           CreateTypeRecord( "26",     false,             false,            true,          false,     false ) },
            { "CreditCard",                               CreateTypeRecord( "18",     false,             false,            false,         true,      true  ) },
            { "CreditLineSecured",                        CreateTypeRecord( "47",     true,              false,            false,         false,     false ) },
            { "DebitCard",                                CreateTypeRecord( "43",     true,              false,            false,         true,      true  ) },
            { "DebtBuyer",                                CreateTypeRecord( "0C",     false,             false,            false,         true,      false ) },
            { "DebtConsolidation",                        CreateTypeRecord( "91",     false,             true,             false,         false,     false ) },
            { "DepositAccountWithOverdraftProtection",    CreateTypeRecord( "8B",     false,             false,            false,         true,      false ) },
            { "Education",                                CreateTypeRecord( "12",     false,             true,             false,         true,      false ) },
            { "FamilySupport",                            CreateTypeRecord( "50",     false,             false,            false,         true,      false ) },
            { "FHAHomeImprovementLoan",                   CreateTypeRecord( "05",     false,             true,             false,         false,     false ) },
            { "FHARealEstateMortgageLoan",                CreateTypeRecord( "19",     false,             false,            true,          false,     false ) },
            { "FlexibleSpendingCreditCard",               CreateTypeRecord( "0G",     false,             false,            false,         false,     true  ) },
            { "GovtBenefit",                              CreateTypeRecord( "75",     false,             true,             false,         true,      false ) },
            { "GovtEmployeeAdvance",                      CreateTypeRecord( "73",     false,             true,             false,         true,      false ) },
            { "GovtFeeForServices",                       CreateTypeRecord( "72",     false,             true,             false,         true,      false ) },
            { "GovtFine",                                 CreateTypeRecord( "71",     false,             true,             false,         true,      false ) },
            // AccountType--------------------------------------------------Code------LineOfCredit-------Installment-------Mortgage-------Open-------Revolving
            { "GovtGrant",                                CreateTypeRecord( "69",     false,             true,             false,         true,      false ) },
            { "GovtMiscDebt",                             CreateTypeRecord( "74",     false,             true,             false,         true,      false ) },
            { "GovtOverpayment",                          CreateTypeRecord( "70",     false,             true,             false,         true,      false ) },
            { "GovtSecuredDirectLoan",                    CreateTypeRecord( "78",     false,             true,             false,         true,      false ) },
            { "GovtSecuredGuaranteedLoan",                CreateTypeRecord( "66",     false,             true,             false,         true,      false ) },
            { "GovtUnsecuredDirectLoan",                  CreateTypeRecord( "67",     false,             true,             false,         true,      false ) },
            { "GovtUnsecuredGuaranteedLoan",              CreateTypeRecord( "65",     false,             true,             false,         true,      false ) },
            { "HomeEquity",                               CreateTypeRecord( "6D",     false,             true,             false,         false,     false ) },
            { "HomeEquityLineOfCredit",                   CreateTypeRecord( "89",     true,              false,            false,         false,     false ) },
            { "HomeImprovement",                          CreateTypeRecord( "04",     false,             true,             false,         false,     false ) },
            { "InstallmentSalesContract",                 CreateTypeRecord( "06",     false,             true,             false,         false,     false ) },
            { "Lease",                                    CreateTypeRecord( "13",     false,             true,             false,         false,     false ) },
            { "LineOfCredit",                             CreateTypeRecord( "15",     true,              false,            false,         false,     false ) },
            { "ManufacturedHousing",                      CreateTypeRecord( "17",     false,             true,             false,         false,     false ) },
            { "MedicalDebt",                              CreateTypeRecord( "90",     false,             true,             false,         true,      false ) },
            { "NoteLoan",                                 CreateTypeRecord( "20",     false,             true,             false,         false,     false ) },
            { "PartiallySecured",                         CreateTypeRecord( "03",     false,             true,             false,         false,     false ) },
            { "RealEstateJrLiensNonPurchaseMoneyFirst",   CreateTypeRecord( "5A",     false,             false,            true,          false,     false ) },
            { "RealEstateSpecificTypeUnknown",            CreateTypeRecord( "08",     false,             false,            true,          false,     false ) },
            { "RecreationalMerchandise",                  CreateTypeRecord( "11",     false,             true,             false,         false,     false ) },
            { "RentalAgreement",                          CreateTypeRecord( "29",     false,             true,             false,         false,     false ) },
            { "ReturnedCheck",                            CreateTypeRecord( "77",     false,             false,            false,         true,      false ) },
            { "SecondMortgage",                           CreateTypeRecord( "5B",     false,             false,            true,          false,     false ) },
            { "Secured",                                  CreateTypeRecord( "02",     false,             true,             false,         false,     false ) },
            { "SecuredCreditCard",                        CreateTypeRecord( "2A",     false,             false,            false,         true,      true  ) },
            { "SecuredHomeImprovement",                   CreateTypeRecord( "9A",     false,             true,             false,         false,     false ) },
            { "Telecommunications",                       CreateTypeRecord( "4D",     false,             false,            false,         true,      false ) },
            { "TimeShareLoan",                            CreateTypeRecord( "0A",     false,             true,             false,         false,     false ) },
            { "Unsecured",                                CreateTypeRecord( "01",     false,             true,             false,         false,     false ) },
            { "USDARealEstateMortgageLoan",               CreateTypeRecord( "2C",     false,             false,            true,          false,     false ) },
            { "UtilityCompany",                           CreateTypeRecord( "92",     false,             false,            false,         true,      false ) },
            { "VetsAdminRealEstateMortgageLoan",          CreateTypeRecord( "25",     false,             false,            true,          false,     false ) }
        };
        #endregion Account Type To Portfolio Mapping

        #region Special Comment To Portfolio Mapping
        public Dictionary<string, Tuple<string, bool, bool, bool, bool, bool>> SpecialCommentPortfolioMap => new Dictionary<string, Tuple<string, bool, bool, bool, bool, bool>>
        {
            // AccountType--------------------------------------------------------Code-----LineOfCredit------Installment------Mortgage------Open------Revolving
            { "None",                                           CreateTypeRecord( "",      true,             true,            true,         true,     true  ) },
            { "PmtManagedByCounselling",                        CreateTypeRecord( "B",     true,             true,            true,         true,     true  ) },
            { "PaidByMakerOrGuarantor",                         CreateTypeRecord( "C",     true,             true,            true,         true,     true  ) },
            { "LoanAssumedByAnotherParty",                      CreateTypeRecord( "H",     false,            true,            true,         false,    false ) },
            { "ElectionOfRemedy",                               CreateTypeRecord( "I",     false,            true,            false,        false,    true  ) },
            { "ClosedAtGuarantorReq",                           CreateTypeRecord( "M",     true,             false,           false,        true,     true  ) },
            { "AccountTransferredToAnotherParty",               CreateTypeRecord( "O",     true,             true,            true,         true,     true  ) },
            { "SplHandlingContactGuarantor",                    CreateTypeRecord( "S",     true,             true,            true,         true,     true  ) },
            { "AdjustmentPending",                              CreateTypeRecord( "V",     true,             true,            true,         true,     true  ) },
            { "DebtBeingPaidThroughInsurance",                  CreateTypeRecord( "AB",    true,             true,            true,         true,     true  ) },
            { "PayingUnderPartialPmtAgreement",                 CreateTypeRecord( "AC",    true,             true,            true,         true,     true  ) },
            { "PurchasedByAnotherCompany",                      CreateTypeRecord( "AH",    true,             true,            true,         true,     true  ) },
            { "RecalledToActiveMilitaryDuty",                   CreateTypeRecord( "AI",    true,             true,            true,         true,     true  ) },
            { "StudentLoanAssignedToGovt",                      CreateTypeRecord( "AL",    true,             true,            false,        false,    false ) },
            { "PaymentsAssuredByWageGarnishment",               CreateTypeRecord( "AM",    true,             true,            true,         true,     true  ) },
            { "AcquiredByFDICOrNCUA",                           CreateTypeRecord( "AN",    true,             true,            true,         true,     true  ) },
            { "SurrenderedAndReinstated",                       CreateTypeRecord( "AO",    false,            true,            false,        false,    true  ) },
            { "CreditLineSuspended",                            CreateTypeRecord( "AP",    true,             false,           false,        true,     true  ) },
            { "ClosedDueToRefinance",                           CreateTypeRecord( "AS",    true,             true,            true,         false,    false ) },
            { "ClosedDueToTransfer",                            CreateTypeRecord( "AT",    false,            true,            false,        false,    false ) },
            { "PaidInFullForLessThanFullBalance",               CreateTypeRecord( "AU",    true,             true,            true,         true,     true  ) },
            { "FirstPmtNeverReceived",                          CreateTypeRecord( "AV",    true,             true,            true,         true,     true  ) },
            { "AffectedByDisaster",                             CreateTypeRecord( "AW",    true,             true,            true,         true,     true  ) },
            { "PaidFromCollateral",                             CreateTypeRecord( "AX",    true,             true,            false,        false,    true  ) },
            { "ReinstatedRepossession",                         CreateTypeRecord( "AZ",    false,            true,            false,        false,    true  ) },
            { "TransferredToRecovery",                          CreateTypeRecord( "BA",    true,             true,            true,         true,     true  ) },
            { "FullTerminationStatusPending",                   CreateTypeRecord( "BB",    false,            true,            false,        false,    false ) },
            { "FullTerminationObligationSatisfied",             CreateTypeRecord( "BC",    false,            true,            false,        false,    false ) },
            { "FullTerminationBalanceOwing",                    CreateTypeRecord( "BD",    false,            true,            false,        false,    false ) },
            // AccountType--------------------------------------------------------Code-----LineOfCredit------Installment------Mortgage------Open------Revolving
            { "EarlyTerminationStatusPending",                  CreateTypeRecord( "BE",    false,            true,            false,        false,    false ) },
            { "EarlyTerminationObligationSatisfied",            CreateTypeRecord( "BF",    false,            true,            false,        false,    false ) },
            { "EarlyTerminationBalanceOwing",                   CreateTypeRecord( "BG",    false,            true,            false,        false,    false ) },
            { "EarlyTerminationInsuranceLoss",                  CreateTypeRecord( "BH",    false,            true,            false,        false,    false ) },
            { "InvoluntaryRepossession",                        CreateTypeRecord( "BI",    false,            true,            false,        false,    false ) },
            { "InvoluntaryRepossessionObligationSatisfied",     CreateTypeRecord( "BJ",    false,            true,            false,        false,    false ) },
            { "InvoluntaryRepossessionBalanceOwing",            CreateTypeRecord( "BK",    false,            true,            false,        false,    false ) },
            { "CreditCardLostOrStolen",                         CreateTypeRecord( "BL",    false,            false,           false,        true,     true) },
            { "PaidByOriginalMerchandiseSeller",                CreateTypeRecord( "BN",    false,            true,            false,        false,    false ) },
            { "ForeclosureProceedingsStarted",                  CreateTypeRecord( "BO",    true,             false,           true,         false,    false ) },
            { "PaidThroughInsurance",                           CreateTypeRecord( "BP",    true,             true,            true,         true,     true  ) },
            { "PrepaidLease",                                   CreateTypeRecord( "BS",    false,            true,            false,        false,    false ) },
            { "InterestPmtOnly",                                CreateTypeRecord( "BT",    true,             true,            true,         false,    true  ) },
            { "GuaranteedOrInsured",                            CreateTypeRecord( "CH",    true,             true,            true,         true,     true  ) },
            { "ClosedDueToInactivity",                          CreateTypeRecord( "CI",    true,             false,           false,        true,     true  ) },
            { "CreditLineNoLongerAvailable",                    CreateTypeRecord( "CJ",    true,             false,           false,        false,    false ) },
            { "CreditLineReducedDueToCollateralDepreciation",   CreateTypeRecord( "CK",    true,             false,           false,        false,    false ) },
            { "Closed",                                         CreateTypeRecord( "CL",    true,             false,           false,        false,    false ) },

            // Documentation seems to have a problem (It says V against Revolving)
            { "CollateralReleasedByCreditorBalanceOwing",       CreateTypeRecord( "CM",    true,             true,            true,         false,    true  ) },
            { "LoanModifiedUnderFedGovtPlan",                   CreateTypeRecord( "CN",    true,             true,            true,         true,     true  ) },
            { "LoanModified",                                   CreateTypeRecord( "CO",    true,             true,            true,         true,     true  ) },
            { "DelinquentAccount",                              CreateTypeRecord( "CS",    false,            false,           false,        true,     false ) },
        };
        #endregion Special Comment To Portfolio Mapping

        #region Compliance Condition Codes
        public Dictionary<string, string> ComplianceConditionCodes => new Dictionary<string, string>
        {
            // Blank, XA, XB, XC, XD, XE, XF, XG, XH, XJ, XR
            { "Null", "" },
            { "AccountClosedAtConsumerRequest", "XA" },  // Account closed at consumer's request
            { "AccountInfoDisputedByBorrower", "XB" },  // Account information disputed by consumer under the Fair Credit Reporting Act
            { "Completed investigation of FCRA dispute - consumer disagrees", "XC" }, // Completed investigation of FCRA dispute - consumer disagrees
            { "Account closed at consumer's request and in dispute under FCRA", "XD" },  // Account closed at consumer's request and in dispute under FCRA
            { "Account closed at consumer's request and dispute investigation completed, consumer disagrees", "XE" },
            { "Account in dispute under Fair Credit Billing Act", "XF" },
            { "FCBA Dispute resolved - consumer disagrees", "XG" },
            { "Account previously in dispute - investigation completed, reported by data furnisher", "XH" },
            { "Account closed at consumer's request and in dispute under FCBA", "XJ" },
            { "Removes the most recently reported Compliance Condition Code", "XR" },
        };
        #endregion Compliance Condition Codes

        #region Consumer Information Indicators
        public Dictionary<string, string> ConsumerInformationIndicators => new Dictionary<string, string>
        {
            // Blank, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Z, 1A, Q, R, V, 2A, S, T, U
            { "None", "" },
            { "Chapter7BankruptcyPetition", "A" },                  // Petition for Chapter 7 Bankruptcy
            { "Chapter11BankruptcyPetition", "B" },                 // Petition for Chapter 11 Bankruptcy
            { "Chapter12BankruptcyPetition", "C" },                 // Petition for Chapter 12 Bankruptcy
            { "Chapter13BankruptcyPetition", "D" },                 // Petition for Chapter 13 Bankruptcy
            { "Chapter7BankruptcyDischarge", "E" },                 // Discharged through Bankruptcy Chapter 7
            { "Chapter11BankruptcyDischarge", "F" },                // Discharged through Bankruptcy Chapter 11
            { "Chapter12BankruptcyDischarge", "G" },                // Discharged through Bankruptcy Chapter 12
            { "Chapter13BankruptcyDischarge", "H" },                // Discharged through Bankruptcy Chapter 13
            { "Chapter7BankruptcyDismissed", "I" },                 // Chapter 7 Bankruptcy Dismissed
            { "Chapter11BankruptcyDismissed", "J" },                // Chapter 11 Bankruptcy Dismissed
            { "Chapter12BankruptcyDismissed", "K" },                // Chapter 12 Bankruptcy Dismissed
            { "Chapter13BankruptcyDismissed", "L" },                // Chapter 13 Bankruptcy Dismissed
            { "Chapter7BankruptcyWithdrawn", "M" },                 // Chapter 7 Bankruptcy Withdrawn
            { "Chapter11BankruptcyWithdrawn", "N" },                // Chapter 11 Bankruptcy Withdrawn
            { "Chapter12BankruptcyWithdrawn", "O" },                // Chapter 12 Bankruptcy Withdrawn
            { "Chapter13BankruptcyWithdrawn", "P" },                // Chapter 13 Bankruptcy Withdrawn
            { "BankruptcyUndesignatedChapter", "Z" },               // Bankruptcy - Undesignated Chapter
            { "PersonalReceivership", "1A" },                       // Personal Receivership
            { "RemovePriorBankruptcyIndicator", "Q" },              // Remove Prior Bankruptcy Indicator
            { "ReaffirmationOfDebt", "R" },                         // Reaffirmation of Debt
            { "Chapter7ReaffirmationOfDebtRescinded", "V" },        // Chapter 7 Reaffirmation Of Debt Rescinded
            { "LeaseAssumption", "2A" },                            // Lease Assumption
            { "RemovePriorDebtReaffirmation", "S" },                // Remove prior debt reaffirmation
            { "GrantorCannotLocateConsumer", "T" },                 // Credit Grantor Cannot Locate Consumer
            { "ConsumerNowLocated", "U" }                           // Consumer Now Located
        };
        #endregion Consumer Information Indicators

        #region ECOA Codes
        public Dictionary<string, string> EcoaCodes => new Dictionary<string, string>
        {
            // 1, 2, 3, 5, 7, T, X, W, Z
            { "Individual", "1" },
            { "JointContractualLiability", "2" },
            { "AuthorizedUser", "3" },
            { "CoMakerOrGuarantor", "5" },
            { "Maker", "7" },
            { "Terminated", "T" },
            { "Deceased", "X" },
            { "Commercial", "W" },
            { "DeleteConsumer", "Z" }
        };
        #endregion ECOA Codes

        #region Generation Codes
        public Dictionary<string, string> GenerationCodes => new Dictionary<string, string>
        {
            // J, S, 2, 3, 4, 5, 6, 7, 8, 9
            { "Junior", "J" },
            { "Senior", "S" },
            { "II", "2" },
            { "III", "3" },
            { "IV", "4" },
            { "V", "5" },
            { "VI", "6" },
            { "VII", "7" },
            { "VIII", "8" },
            { "IX", "9" },
        };
        #endregion Generation Codes

        #region Interest Type Indicators
        public Dictionary<string, string> InterestTypeIndicators => new Dictionary<string, string>
        {
            { "Unknown", "" },
            { "Fixed", "F" },
            { "Variable", "V" }
        };
        #endregion Interest Type Indicators

        #region Payment Ratings
        public Dictionary<string, string> PaymentRatings => new Dictionary<string, string>
        {
            // 0, 1, 2, 3, 4, 5, 6, G, L
            { "0To29DaysPastDueDate", "0" },
            { "30To59DaysPastDueDate", "1" },
            { "60To89DaysPastDueDate", "2" },
            { "90To119DaysPastDueDate", "3" },
            { "120To149DaysPastDueDate", "4" },
            { "150To179DaysPastDueDate", "5" },
            { "180OrMoreDaysPastDueDate ", "6" },
            { "Collection", "G" },
            { "ChargeOff", "L" }
        };
        #endregion Payment Ratings

        #region Portfolio Types
        public Dictionary<string, string> PortfolioTypes => new Dictionary<string, string>
        {
            { "LineOfCredit", "C" },
            { "Installment", "I" },
            { "Mortgage", "M" },
            { "Open", "O" },
            { "Revolving", "R" }
        };
        #endregion Portfolio Types

        #region Residence Codes
        public Dictionary<string, string> ResidenceCodes => new Dictionary<string, string>
        {
            { "Own", "O" },
            { "Rented", "R" }
        };
        #endregion Residence Codes

        #region Terms Frequency Codes
        public Dictionary<string, string> TermsFrequencyCodes => new Dictionary<string, string>
        {
            { "Deferred", "D" },
            { "SinglePaymentLoan", "P" },
            { "Weekly", "W" },
            { "Biweekly", "B" },
            { "SemiMonthly", "E" },
            { "Monthly", "M" },
            { "Bimonthly", "L" },
            { "Quarterly", "Q" },
            { "Trianually", "T" },
            { "Semianually", "S" },
            { "Anually", "Y" },
        };

        #endregion Terms Frequency Codes

        #region Transaction Types
        public Dictionary<string, string> TransactionTypes => new Dictionary<string, string>
        {
            { "NewAccount", "1" },
            { "NewBorrower", "1" },
            { "NameChange", "2" },
            { "AddressChange", "3" },
            { "SSNChange", "5" },
            { "NameAndAddressChange", "6" },
            { "NameAndSSNChange", "8" },
            { "AddressAndSSNChange", "9" },
            { "NameAndAddressOrSSNChange", "A" } // TODO: It is actually Name and Address and/or SSN change. Figure out a better name.
        };

        #endregion Transaction Types

        #region Creditor Classification Codes
        public Dictionary<string, string> CreditorClassificationCodes => new Dictionary<string, string>
        {
            { "Retail", "01" },
            { "Medical", "02" },
            { "OilCompany", "03" },
            { "Government", "04" },
            { "PersonalServices", "05" },
            { "Insurance", "06" },
            { "Education", "07" },
            { "Banking", "08" },
            { "Rental", "09" },
            { "Utilities", "10" },
            { "Cable", "11" },
            { "Financial", "12" },
            { "CreditUnion", "13" },
            { "Automotive", "14" },
            { "CheckGuarantee", "15" },
        };
        #endregion Creditor Classification Codes

        #region Agency Identifiers
        public Dictionary<string, string> AgencyIdentifiers => new Dictionary<string, string>
        {
            { "NotApplicable", "00" },
            { "FannieMae", "01" },
            { "FreddieMac", "02" }
        };
        #endregion Agency Identifiers

        #region Specialized Payment Indicators
        public Dictionary<string, string> SpecializedPaymentIndicators => new Dictionary<string, string>
        {
            { "BalloonPayment", "01" },
            { "DeferredPayment", "02" }
        };
        #endregion Specialized Payment Indicators

        #region Change Indicators
        public Dictionary<string, string> ChangeIndicators => new Dictionary<string, string>
        {
            { "ConsumerAccountNumberOnly", "1" },
            { "IdentificationNumberOnly", "2" },
            { "ConsumerAccountNumberAndIdentificationNumber", "3" }
        };
        #endregion Change Indicators

        #region States
        public Dictionary<string, string> States => new Dictionary<string, string>
        {
            { "AL", "Alabama" },
            { "ND", "North Dakota" },
            { "MP", "Northern Mariana Islands" },
            { "AK", "Alaska" },
            { "AS", "American Samoa" },
            { "AZ", "Arizona" },
            { "AR", "Arkansas" },
            { "CA", "California" },
            { "CO", "Colorado" },
            { "CT", "Connecticut" },
            { "DE", "Delaware" },
            { "DC", "District of Columbia" },
            { "FM", "Federated States of Micronesia" },
            { "FL", "Florida" },
            { "GA", "Georgia" },
            { "GU", "Guam" },
            { "UT", "Utah" },
            { "HI", "Hawaii" },
            { "ID", "Idaho" },
            { "IL", "Illinois" },
            { "IM", "Indiana" },
            { "IA", "Iowa" },
            { "KS", "Kansas" },
            { "KY", "Kentucky" },
            { "LA", "Louisiana" },
            { "ME", "Maine" },
            { "MH", "Marshall Islands" },
            { "MD", "Maryland" },
            { "MA", "Massachusetts" },
            { "MI", "Michigan" },
            { "MN", "Minnesota" },
            { "MS", "Mississippi" },
            { "MO", "Missouri" },
            { "MT", "Montana" },
            { "NE", "Nebraska" },
            { "NV", "Nevada" },
            { "NH", "New Hampshire" },
            { "NJ", "New Jersey" },
            { "NM", "New Mexico" },
            { "NY", "New York" },
            { "NC", "North Carolina" },
            { "OH", "Ohio" },
            { "OK", "Oklahoma" },
            { "OR", "Oregon" },
            { "PW", "Palau" },
            { "PA", "Pennsylvania" },
            { "PR", "Puerto Rico" },
            { "RI", "Rhode Island" },
            { "SC", "South Carolina" },
            { "SD", "South Dakota" },
            { "TN", "Tennessee" },
            { "TX", "Texas" },
            { "VT", "Vermont" },
            { "VA", "Virginia" },
            { "VI", "Virgin Islands" },
            { "WA", "Washington" },
            { "WV", "West Virginia" },
            { "WI", "Wisconsin" },
            { "WY", "Wyoming" },
            { "AA", "Military in the Americas other than Canada" },
            { "AE", "Military in Europe, Middle East, Africa, Canada" },
            { "AP", "Military in the Pacific Theater" },
        };
        #endregion States

        #region Countries
        public Dictionary<string, string> Countries => new Dictionary<string, string>
        {
            { "AF", "Afghanistan" },
            { "AN", "Albania" },
            { "DZ", "Algeria" },
            { "AD", "Andorra" },
            { "AO", "Angola"  },
            { "Al", "Anguilla" },
            { "AG", "Antigua & Barbuda" },
            { "AT", "Argentina" },
            { "RM", "Armenia" },
            { "AW", "Aruba" },
            { "AS", "Ascension" },
            { "AU", "Australia" },
            { "DF", "Austria" },
            { "AJ", "Azerbaijan" },
            { "AX", "Azores" },
            { "BS", "Bahamas" },
            { "BH", "Bahrain" },
            { "BD", "Bangladesh" },
            { "BB", "Barbados" },
            { "BL", "Belarus" },
            { "BE", "Belgium" },
            { "BZ", "Belize" },
            { "BJ", "Benin" },
            { "BU", "Bermuda" },
            { "BM", "Bhutan" },
            { "BO", "Bolivia" },
            { "BX", "Bosnia & Herzegovina" },
            { "BW", "Botswana" },
            { "BR", "Brazil" },
            { "VG", "British Virgin Islands" },
            { "BN", "Brunei" },
            { "BG", "Bulgaria" },
            { "BF", "Burkina Faso" },
            { "BI", "Burundi" },
            { "KA", "Cambodia" },
            { "CM", "Cameroon" },
            { "CN", "Canada" },
            { "CV", "Cape Verde" },
            { "CU", "Carriacou" },
            { "CI", "Cayman Islands" },
            { "CF", "Central African Republic" },
            { "CD", "Chad" },
            { "CL", "Chile" },
            { "CP", "China(Peking)" },
            { "CB", "Colombia" },
            { "CJ", "Comoros" },
            { "CG", "Congo" },
            { "CC", "Corsica" },
            { "CR", "Costa Rica" },
            { "HX", "Croatia" },
            { "HR", "Cuba" },
            { "CY", "Cyprus" },
            { "CZ", "Czech Republic" },
            { "ZR", "Democratic Republic of Congo" },
            { "DK", "Denmark" },
            { "DJ", "Djibouti" },
            { "DM", "Dominica" },
            { "DO", "Dominican Republic" },
            { "EM", "East Timor" },
            { "EC", "Ecuador" },
            { "EG", "Egypt" },
            { "SV", "El Salvador" },
            { "GQ", "Equatorial Guinea" },
            { "SU", "Estonia" },
            { "ET", "Ethiopia" },
            { "FA", "Falkland Islands" },
            { "FE", "Faroe Islands" },
            { "FJ", "Fiji" },
            { "Fl", "Finland" },
            { "FR", "France," },
            { "GF", "French Guiana" },
            { "FP", "French Polynesia" },
            { "GB", "Gabon" },
            { "GM", "Gambia" },
            { "DW", "Germany" },
            { "GH", "Ghana" },
            { "GI", "Gibraltar" },
            { "GD", "Granada" },
            { "GR", "Greece" },
            { "GE", "Greenland" },
            { "GP", "Guadeloupe" },
            { "GT", "Guatemala" },
            { "GN", "Guinea" },
            { "GW", "Guinea-Bissau" },
            { "GY", "Guyana" },
            { "HA", "Haiti" },
            { "HN", "Honduras" },
            { "HU", "Hungary" },
            { "IS", "Iceland" },
            { "IB", "India" },
            { "IF", "Indonesia" },
            { "IR", "Iran" },
            { "IQ", "Iraq" },
            { "IE", "Ireland" },
            { "IG", "Israel" },
            { "IT", "Italy" },
            { "IC", "Ivory Coast" },
            { "JM", "Jamaica" },
            { "JP", "Japan" },
            { "JO", "Jordan" },
            { "KZ", "Kazakhstan" },
            { "KE", "Kenya" },
            { "KI", "Kiribati" },
            { "KX", "Korea(North)" },
            { "KR", "Korea(South)" },
            { "KW", "Kuwait" },
            { "KG", "Kyrgyzstan" },
            { "LO", "Laos" },
            { "LX", "Latvia" },
            { "LB", "Lebanon" },
            { "LE", "Leeward Islands" },
            { "LS", "Lesotho" },
            { "LR", "Liberia" },
            { "LV", "Libya" },
            { "CH", "Liechtenstein" },
            { "LT", "Lithuania" },
            { "LU", "Luxembourg" },
            { "MJ", "Macao" },
            { "MH", "Macedonia" },
            { "MG", "Madagascar" },
            { "MB", "Madeira" },
            { "MW", "Malawi" },
            { "MY", "Malaysia" },
            { "MV", "Maldives" },
            { "ML", "Mali" },
            { "MF", "Malta" },
            { "MQ", "Martinique" },
            { "MR", "Mauritania" },
            { "MU", "Mauritius" },
            { "MX", "Mexico" },
            { "LD", "Moldova" },
            { "AC", "Monaco" },
            { "MC", "Mongolia" },
            { "MK", "Montserrat" },
            { "RC", "Morocco" },
            { "MZ", "Mozambique" },
            { "NB", "Namibia" },
            { "NA", "Nauru" },
            { "NP", "Nepal" },
            { "NN", "Netherlands Antilles" },
            { "NL", "Netherlands" },
            { "NW", "New Caledonia" },
            { "NZ", "New Zealand" },
            { "NI", "Nicaragua" },
            { "NR", "Niger" },
            { "NG", "Nigeria" },
            { "NO", "Norway" },
            { "OM", "Oman" },
            { "PK", "Pakistan" },
            { "PM", "Panama" },
            { "PG", "Papua New Guinea" },
            { "PY", "Paraguay" },
            { "PU", "Peru" },
            { "PH", "Philippines" },
            { "PS", "Pitcairn Islands" },
            { "PL", "Poland" },
            { "PT", "Portugal" },
            { "QA", "Qatar" },
            { "GX", "Republic of Georgia" },
            { "RE", "Reunion Island" },
            { "RO", "Romania" },
            { "RU", "Russia" },
            { "RW", "Rwanda" },
            { "SH", "Saint Helena" },
            { "KN", "Saint Kitts & Nevis" },
            { "LC", "Saint Lucia" },
            { "SP", "Saint Pierre & Miquelon" },
            { "SF", "Saint Vincent & the Grenadines" },
            { "SM", "San Marino" },
            { "ST", "Santa Cruz Islands" },
            { "MP", "Sao Tome & Principe" },
            { "SA", "Saudi Arabia" },
            { "SN", "Senegal" },
            { "SX", "Serbia and Montenegro" },
            { "YC", "Seychelles" },
            { "SL", "Sierra Leone" },
            { "SG", "Singapore" },
            { "VK", "Slovakia" },
            { "XN", "Slovenia" },
            { "SI", "Solomon Islands" },
            { "SO", "Somalia" },
            { "ZA", "South Africa" },
            { "ES", "Spain" },
            { "LK", "Sri Lanka" },
            { "SB", "Sudan" },
            { "SR", "Suriname" },
            { "SZ", "Swaziland" },
            { "SE", "Sweden" },
            { "SW", "Switzerland" },
            { "SY", "Syria" },
            { "TK", "Tadzhikistan" },
            { "TW", "Taiwan" },
            { "TZ", "Tanzania" },
            { "TH", "Thailand" },
            { "TG", "Togo" },
            { "TA", "Tonga" },
            { "TT", "Trinidad & Tobago" },
            { "TD", "Tristan da Cunha" },
            { "TU", "Tunisia" },
            { "TR", "Turkey" },
            { "TM", "Turkmenistan" },
            { "TC", "Turks & Caicos Islands" },
            { "TV", "Tuvalu" },
            { "UG", "Uganda" },
            { "UA", "Ukraine" },
            { "BK", "Union of Myanmar (Burma)" },
            { "UM", "United Arab Emirates" },
            { "UK", "United Kingdom" },
            { "US", "United States" },
            { "UY", "Uruguay" },
            { "UZ", "Uzbekistan" },
            { "VU", "Vanuatu" },
            { "VC", "Vatican City State" },
            { "VE", "Venezuela" },
            { "VN", "Vietnam" },
            { "WT", "Wallis/Futuna Island" },
            { "WS", "Western Samoa" },
            { "YE", "Yemen" },
            { "ZM", "Zambia" },
            { "ZW", "Zimbabwe" }
        };

        #endregion Countries

        #region Payment History
        public Dictionary<string, string> PaymentStatuses => new Dictionary<string, string>
        {
            // 0, 1, 2, 3, 4, 5, 6, B, D, E, G, H, J, K, L
            { "0To29DaysPastDueDate", "0" },
            { "30To59DaysPastDueDate", "1" },
            { "60To89DaysPastDueDate", "2" },
            { "90To119DaysPastDueDate", "3" },
            { "120To149DaysPastDueDate", "4" },
            { "150To179DaysPastDueDate", "5" },
            { "180OrMoreDaysPastDueDate ", "6" },
            { "PriorHistoryUnavailable", "B" },         // No payment history available prior to this time.A "B" may not be embedded within other values.
            { "HistoryUnavailableThisMonth", "D" },     // No payment history available this month. A "D" may be embedded in the payment pattern.
            { "ZeroBalanceAndCurrentAccount", "E" },    // Zero balance and current account
            { "Collection", "G" },                      // Collection
            { "ForeclosureCompleted", "H" },            // Foreclosure Completed
            { "VoluntarySurrender", "J" },              // Voluntary Surrender
            { "Repossession", "K" },                    // Repossession
            { "ChargeOff", "L" }                        // Charge-off
        };
        #endregion Payment History

        #region AddressIndicators
        public Dictionary<string, string> AddressIndicators => new Dictionary<string, string>
        {
            { "Unknown", "" },                 // Address indicator not available/unknown
            { "Confirmed", "C" },              //C = Confirmed / Verified address. Note: Value 'C' enables reporting a confirmed or verified address after receiving an address discrepancy  notification from a consumer reporting agency.
            { "PrimaryBorrowerAddress", "Y" }, //Y = Known to be address of primary consumer
            { "NotConfirmed", "N" },           //N = Not confirmed address
            { "Business", "B" },               //B = Business address — not consumer's residence
            { "NonDeliverable", "U" },         //U = Non - deliverable address / Returned mail
            { "DataReporter", "D" },           //D =  Data reporter's default address
            { "Military", "M" },               //M = Military address
            { "Secondary", "S" },              //S = Secondary Address
            { "BillPayerService", "P" }        //P =  Bill Payer Service — not consumer's residence
        };

        #endregion AddressIndicators








        //Portfolio Indicator(part of k2):
        //1, 2, 9 (the other field in k2 has validation based on this)
        //public List<string> PortfolioIndicator => new List<string> { "Sale", "Purchase", "RemovePrevious" };

        //Special Comment Codes:
        //Will come back to this later

        //State:
        //Can get this later

        //Terms Duration:
        //Along with validation, one of the folliwing has to be reported: LOC, 001, REV(see documentation before implementing this)
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class BorrowerInfoValidator : ViewModelValidator<BorrowerInfo>
    {
        private BorrowerInfo BorrowerInfo { get; set; }
        private IValidationContext Context { get; set; }
        public BorrowerInfoValidator(BorrowerInfo borrowerInfo, IValidationContext context) : base(borrowerInfo, context)
        {
            BorrowerInfo = borrowerInfo;
            Context = context;

            RuleFor(data => data.Contact).SetValidator(new ContactValidator(BorrowerInfo.Contact, context));

            RuleFor(data => data.ECOACode)
                                .Must(BeValidEcoaCode)
                                .WithMessage("ECOACode has an invalid value.");

            RuleFor(data => data.ComplianceConditionCode)
                                .Must(BeValidComplianceConditionCode)
                                .WithMessage("ComplianceConditionCode has an invalid value.");

            RuleFor(data => data.InterestTypeIndicator)
                                .Must(BeValidInterestTypeIndicator)
                                .WithMessage("Invalid InterestTypeIndicator value. Allowed values are 'fixed' and 'variable'.");

            RuleFor(data => data.PaymentRating)
                                .Must(BeValidPaymentRating)
                                .WithMessage("PaymentRating has an invalid value.");

            RuleFor(data => data.PaymentRating)
                                .NotEmpty()
                                .When(AccountStatusMakesPaymentRatingMandatory)
                                .WithMessage($"PaymentRating must be specified if the account status is {Context.AccountStatusCode} ");

            RuleFor(data => data.TermsFrequency)
                                .Must(BeValidTermsFrequency)
                                .WithMessage("TermsFrequency has an invalid value.");

            RuleFor(data => data.TransactionType)
                                .NotEmpty()
                                .Must(BeValidTransactionType)
                                .WithMessage("TransactionType has an invalid value.");
        }


        // The BorrowerInfo param is there only to comply with the func delegate signature of the When() method. Internally, we check for the 
        // AccountStatusCode value of the context.
        private bool AccountStatusMakesPaymentRatingMandatory(BorrowerInfo borrowerInfo)
        {
            // for these status codes, payment rating is mandatory
            switch(Context.AccountStatusCode)
            {
                case "AccountTransferred":
                case "Paid":
                case "Closed":
                case "ZeroBalance":
                case "ForeclosureStartedPaidInFull":
                case "InsuranceClaimFiled":
                case "DeedReceivedForForeclosure":
                case "ForeclosureCompleted":
                case "VoluntarySurrenderBalanceDue":
                    return true;
            }

            return false;
        }

        private bool BeValidEcoaCode(string code)
        {
            return Context.Lookups.EcoaCodes.ContainsKey(code);
        }

        private bool BeValidComplianceConditionCode(string code)
        {
            return Context.Lookups.ComplianceConditionCodes.ContainsKey(code);
        }

        private bool BeValidInterestTypeIndicator(string interestType)
        {
            return interestType.ToLower() == "fixed" || interestType.ToLower() == "variable";
        }

        private bool BeValidPaymentRating(string rating)
        {
            if (!string.IsNullOrEmpty(rating))
            {
                return Context.Lookups.PaymentRatings.ContainsKey(rating);
            }

            return true;
        }

        public bool BeValidTermsFrequency(string frequency)
        {
            return Context.Lookups.TermsFrequencyCodes.ContainsKey(frequency);
        }

        // can we handle this via routing?
        private void ValidatePortfolioType()
        {
            //throw new NotImplementedException();
        }

        private void ValidateSpecialComment()
        {
            RuleFor(data => data.SpecialComment)
                                .NotEmpty()
                                    .WithMessage("SpecialComment is required.")
                                .Must(BeValidSpecialComment)
                                    .WithMessage("Invalid special comment - unrecognized value.")
                                .Must(BeValidSpecialCommentForPortfolio)
                                    .WithMessage($"The special comment {BorrowerInfo.SpecialComment} should not be applied to the {Context.Portfolio} portfolio.");
        }

        private bool BeValidSpecialComment(string arg)
        {
            return Context.Lookups.SpecialCommentPortfolioMap.ContainsKey(arg);          
        }

        private bool BeValidSpecialCommentForPortfolio(string portfolio)
        {
            var record = Context.Lookups.AccountTypePortfolioMap[portfolio];

            switch (Context.Portfolio)
            {
                case "LineOfCredit":
                    return record.Item2;
                case "Installment":
                    return record.Item3;
                case "Mortgage":
                    return record.Item4;
                case "Open":
                    return record.Item5;
                case "Revolving":
                    return record.Item6;
                default:
                    return false;
            }
        }

        private bool BeValidTransactionType(string transactionType)
        {
            return Context.Lookups.TransactionTypes.ContainsKey(transactionType);
        }
    }
}
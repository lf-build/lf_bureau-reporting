﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class BaseSegmentValidator : AbstractValidator<BaseSegment>
    {
        private const string SegmentName = "Base Segment";

        public BaseSegmentValidator()
        {
            RuleFor(segment => segment.BlockDescriptorWord).NotEmpty().WithMessage($"{SegmentName} - BlockDescriptorWord cannot not be empty.");
            RuleFor(segment => segment.RecordDescriptorWord).NotEmpty().WithMessage($"{SegmentName} - RecordDescriptorWord cannot not be empty.");
            RuleFor(segment => segment.IdentificationNumber).NotEmpty().WithMessage($"{SegmentName} - IdentificationNumber cannot not be empty.");
            RuleFor(segment => segment.CycleIdentifier).NotEmpty().WithMessage($"{SegmentName} - CycleIdentifier cannot not be empty.");
            RuleFor(segment => segment.ConsumerAccountNumber).NotEmpty().WithMessage($"{SegmentName} - ConsumerAccountNumber cannot not be empty.");
            RuleFor(segment => segment.PortfolioType).NotEmpty().WithMessage($"{SegmentName} - PortfolioType cannot not be empty.");
            RuleFor(segment => segment.AccountType).NotEmpty().WithMessage($"{SegmentName} - AccountType cannot not be empty.");
            RuleFor(segment => segment.DateOpened).NotNull().WithMessage($"{SegmentName} - DateOpened cannot not be empty.");
            RuleFor(segment => segment.CreditLimit).NotNull().WithMessage($"{SegmentName} - CreditLimit cannot not be empty.");
            RuleFor(segment => segment.HighestCredit).NotNull().WithMessage($"{SegmentName} - HighestCredit cannot be empty.");
            RuleFor(segment => segment.TermsDuration).NotNull().WithMessage($"{SegmentName} - TermsDuration cannot be empty.");
            RuleFor(segment => segment.TermsFrequency).NotNull().WithMessage($"{SegmentName} - TermsFrequency cannot be empty.");
            RuleFor(segment => segment.ScheduledMonthlyPaymentAmount).NotNull().WithMessage($"{SegmentName} - ScheduledMonthlyPaymentAmount cannot be empty.");
            RuleFor(segment => segment.AccountStatus).NotEmpty().WithMessage($"{SegmentName} - AccountStatus cannot not be empty.");
            RuleFor(segment => segment.PaymentRating).NotEmpty().WithMessage($"{SegmentName} - PaymentRating cannot not be empty.");
            RuleFor(segment => segment.PaymentHistoryProfile).NotEmpty().WithMessage($"{SegmentName} - PaymentHistoryProfile cannot not be empty.");
            RuleFor(segment => segment.ComplianceConditionCode).NotEmpty().WithMessage($"{SegmentName} - ComplianceConditionCode cannot not be empty.");
            RuleFor(segment => segment.CurrentBalance).NotEmpty().WithMessage($"{SegmentName} - CurrentBalance cannot be empty.");
            RuleFor(segment => segment.AmountPastDue).NotEmpty().WithMessage($"{SegmentName} - AmountPastDue cannot be empty.");
            RuleFor(segment => segment.OriginalChargeoffAmount).NotEmpty().WithMessage($"{SegmentName} - OriginalChargeOffAmount cannot be empty.");
            RuleFor(segment => segment.BillingDate).NotNull().WithMessage($"{SegmentName} - BillingDate cannot not be empty.");
            RuleFor(segment => segment.DateofFirstDelinquency).NotEmpty().WithMessage($"{SegmentName} - DateofFirstDelinquency cannot not be empty.");
            RuleFor(segment => segment.DateClosed).NotNull().WithMessage($"{SegmentName} - DateClosed cannot not be empty.");
            RuleFor(segment => segment.DateofLastPayment).NotNull().WithMessage($"{SegmentName} - DateofLastPayment cannot not be empty.");
            RuleFor(segment => segment.LastName).NotEmpty().WithMessage($"{SegmentName} - LastName cannot not be empty.");
            RuleFor(segment => segment.FirstName).NotEmpty().WithMessage($"{SegmentName} - FirstName cannot not be empty.");
            RuleFor(segment => segment.MiddleName).NotEmpty().WithMessage($"{SegmentName} - MiddleName cannot not be empty.");
            RuleFor(segment => segment.GenerationCode).NotEmpty().WithMessage($"{SegmentName} - GenerationCode cannot not be empty.");
            RuleFor(segment => segment.SocialSecurityNumber).NotEmpty().WithMessage($"{SegmentName} - SocialSecurityNumber cannot not be empty.");
            RuleFor(segment => segment.DateOfBirth).NotNull().WithMessage($"{SegmentName} - DateOfBirth cannot not be empty.");
            RuleFor(segment => segment.ConsumerInformationIndicator).NotEmpty().WithMessage($"{SegmentName} - ConsumerInformationIndicator cannot not be empty.");
            RuleFor(segment => segment.FirstLineOfAddress).NotEmpty().WithMessage($"{SegmentName} - FirstLineOfAddress cannot not be empty.");
            RuleFor(segment => segment.SecondLineOfAddress).NotEmpty().WithMessage($"{SegmentName} - SecondLineOfAddress cannot not be empty.");
            RuleFor(segment => segment.City).NotEmpty().WithMessage($"{SegmentName} - City cannot not be empty.");
            RuleFor(segment => segment.State).NotEmpty().WithMessage($"{SegmentName} - State cannot not be empty.");
            RuleFor(segment => segment.ZipCode).NotEmpty().WithMessage($"{SegmentName} - ZipCode cannot not be empty.");
        }
    }
}

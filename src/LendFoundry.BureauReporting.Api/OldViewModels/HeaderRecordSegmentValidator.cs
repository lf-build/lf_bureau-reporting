﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class HeaderRecordSegmentValidator : AbstractValidator<HeaderRecordSegment>
    {
        private const string SegmentName = "Header Record";
        public HeaderRecordSegmentValidator()
        {
            RuleFor(segment => segment.BlockDescriptorWord).NotEmpty().WithMessage($"{SegmentName} - BlockDescriptorWord cannot not be empty.");
            RuleFor(segment => segment.RecordDescriptorWord).NotEmpty().WithMessage($"{SegmentName} - RecordDescriptorWord cannot not be empty.");
            RuleFor(segment => segment.RecordIdentifier).NotEmpty().WithMessage($"{SegmentName} - RecordIdentifier cannot not be empty.");
            RuleFor(segment => segment.RecordDescriptorWord).NotEmpty().WithMessage($"{SegmentName} - RecordDescriptorWord cannot not be empty.");
            RuleFor(segment => segment.InnovisProgramIdentifier).NotEmpty().WithMessage($"{SegmentName} - InnovisProgramIdentifier cannot not be empty.");
            RuleFor(segment => segment.EquifaxProgramIdentifier).NotEmpty().WithMessage($"{SegmentName} - EquifaxProgramIdentifier cannot not be empty.");
            RuleFor(segment => segment.ExperianProgramIdentifier).NotEmpty().WithMessage($"{SegmentName} - ExperianProgramIdentifier cannot not be empty.");
            RuleFor(segment => segment.TransUnionProgramIdentifier).NotEmpty().WithMessage($"{SegmentName} - TransUnionProgramIdentifier cannot not be empty.");
            RuleFor(segment => segment.ReporterName).NotEmpty().WithMessage($"{SegmentName} - ReporterName cannot not be empty.");
            RuleFor(segment => segment.ReporterAddress).NotEmpty().WithMessage($"{SegmentName} - ReporterAddress cannot not be empty.");
            RuleFor(segment => segment.SoftwareVendorName).NotEmpty().WithMessage($"{SegmentName} - SoftwareVendorName cannot not be empty.");
            RuleFor(segment => segment.SoftwareVersionNumber).NotEmpty().WithMessage($"{SegmentName} - SoftwareVersionNumber cannot not be empty.");
            RuleFor(segment => segment.ActivityDate).NotNull().WithMessage($"{SegmentName} - ActivityDate cannot be null.");
            RuleFor(segment => segment.DateCreated).NotNull().WithMessage($"{SegmentName} - DateCreated cannot be null.");
            RuleFor(segment => segment.ProgramDate).NotNull().WithMessage($"{SegmentName} - ProgramDate cannot be null.");
            RuleFor(segment => segment.ProgramRevisionDate).NotNull().WithMessage($"{SegmentName} - ProgramRevisionDate cannot be null.");
        }
    }
}



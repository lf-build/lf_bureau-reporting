﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using FluentValidation.Results;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.BureauReporting.Core.Abstractions;
using LendFoundry.BureauReporting.Core.Domain.Lookups;
using LendFoundry.BureauReporting.Api.Models;
using AutoMapper;
using LendFoundry.BureauReporting.Core.Domain;
using Microsoft.AspNet.Http;
using Microsoft.Net.Http.Headers;
using LendFoundry.BureauReporting.Api.Models.Account;
using LendFoundry.BureauReporting.Api.Models.Payments;
using LendFoundry.BureauReporting.Api.Models.Borrower;
using LendFoundry.BureauReporting.Api.Models.Common;
using LendFoundry.BureauReporting.Api.Models.Loan;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LendFoundry.BureauReporting.Api.Controllers
{
    [Route("/")]
    public class ReportDataController : Controller
    {
        private IReportDataService ReportDataService { get; }
        private ITenantTime TenantTime { get; }
        private ILookups Lookups { get; }

        public ReportDataController(IReportDataService reportDataService, ILookups lookups, ILogger logger, ITokenReader tokenReader) // , ITenantTime tenantTime)
        {
            ReportDataService = reportDataService;
            //TenantTime = tenantTime;
            Lookups = lookups;

            Lookups = new Lookups();
        }

        [HttpPut]
        public IActionResult AddBureauReportData([FromBody]ICollection<ReportDataModel> model)
        {
            var reportData = Mapper.Map<ICollection<ReportDataModel>, ICollection<ReportData>>(model);
            ReportDataService.AddBureauReportData(reportData);
            return new HttpOkResult();
        }

        //[HttpPut, HttpPost]
        //public IActionResult AddBureauReportData()
        //{
        //    throw new NotImplementedException();
        //}

        [HttpPost]
        public IActionResult ImportFileData(IList<IFormFile> files)
        {
            foreach (var file in files)
            {
                var fileName = ContentDispositionHeaderValue
                  .Parse(file.ContentDisposition)
                  .FileName
                  .Trim('"');
                var filePath = string.Empty; // HostingEnvironment.ApplicationBasePath + "\\Documents\\" + DateTime.Now.ToString("yyyyddMHHmmss") + fileName;
                file.SaveAs(filePath);
            }

            ReportDataService.ImportFileData();
            return new HttpOkResult();
        }

        [HttpPost("/send")]
        public IActionResult SubmitReport()
        {
            ReportDataService.SubmitReport();
            return new HttpOkResult();
        }



        //private IValidationContext CreateValidationContext(ReportData reportData)
        //{
        //    var context = new ValidationContext();

        //    if (reportData != null && reportData.AccountInfo != null)
        //    {
        //        context.AccountNumber = reportData.AccountInfo.AccountNumber;
        //        context.AccountStatusCode = reportData.AccountInfo.AccountStatus;
        //        context.AccountOpeningDate = reportData.AccountInfo.DateOpened;
        //        context.AccountTypeCode = reportData.AccountInfo.AccountType;
        //        context.Portfolio = reportData.AccountInfo.Portfolio;
        //        context.TermsFrequency = reportData.BorrowerInfo.TermsFrequency;
        //        context.TenantTime = TenantTime;
        //    }

        //    context.Lookups = Lookups;
        //    return context;
        //}

        //[HttpPut("/{portfolio}")]
        //public IActionResult AddBureauReportData([FromRoute]string portfolio, [FromBody]ICollection<ReportDataModel> reportData)
        //{
        //    switch (portfolio)
        //    {
        //        case "LineOfCredit":
        //        case "Installment":
        //        case "Mortgate":
        //        case "Open":
        //        case "Revolving":
        //            break;
        //        default:
        //            return HttpBadRequest("Invalid portfolio. Expected: LineOfCredit, Installment, Mortgage, Open, Revolving.");
        //    }

        //    // TODO: Use Parallel.Foreach()
        //    //foreach (var model in reportData)
        //    //{
        //    //    if(model != null && model.AccountInfo != null)
        //    //    {
        //    //        model.AccountInfo.Portfolio = portfolio;
        //    //    }

        //    //    var context = CreateValidationContext(model);

        //    //    var validator = new ReportDataValidator(model, Lookups, context);
        //    //    var validationResult = validator.Validate(model);

        //    //    if (!validationResult.IsValid)
        //    //    {
        //    //        return HttpBadRequest(validationResult.Errors);
        //    //    }
        //    //}


        //    return null;
        //}






        [HttpGet]
        public string GetJson()
        {
            var data = new ReportDataModel();
            data.AccountInfo = new AccountInfoModel();
            data.AccountInfo.TotalAmountDue = new AmountModel();
            data.AccountInfo.AccountStatus = new GeneralAccountStatusModel();
            data.AccountInfo.AccountStatus.TotalAmountDue = new AmountModel();

            var borrowerInfoModel = new BorrowerInfoModel();
            borrowerInfoModel.Address = new AddressModel();
            borrowerInfoModel.Borrower = new PersonModel();

            data.BorrowerInfo = new List<BorrowerInfoModel> { borrowerInfoModel };

            data.LoanInfo = new LoanInfoModel();
            data.LoanInfo.Product = new ProductInfoModel();

            return Newtonsoft.Json.JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented);
        }
    }
}

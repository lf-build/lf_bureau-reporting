﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class PaymentStatus
    {
        public int MonthNumber { get; set; }
        public string Status { get; set; }
    }
}

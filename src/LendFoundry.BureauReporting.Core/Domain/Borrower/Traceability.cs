﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Borrower
{
    public class Traceability
    {
        public string OverPhone { get; set; }
        public string OverEmail { get; set; }
        public string OverPost { get; set; }
        public bool InSkiptrace { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class AccountPurchaseSaleInfoValidator : ViewModelValidator<AccountPurchaseSaleInfo>
    {
        private AccountPurchaseSaleInfo AccountPurchaseSaleInfo { get; set; }
        private IValidationContext Context { get; set; }

        private const string purchase = "Purchase";
        private const string sale = "Sale";
        private const string removePrevious = "RemovePrevious";
        public AccountPurchaseSaleInfoValidator(AccountPurchaseSaleInfo accountPurchaseSaleInfo, IValidationContext context) : base(accountPurchaseSaleInfo, context)
        {
            AccountPurchaseSaleInfo = accountPurchaseSaleInfo;
            Context = context;

            RuleFor(data => data.PortfolioIndicator)
                .NotEmpty()
                    .WithMessage("PortfolioIndicator is required.")
                .Must(BeValidLookupValue)
                    .WithMessage($"Account number {Context.AccountNumber} - The portfolio indicator is invalid. Allowed values are 'Sale', 'Purchase', 'RemovePrevious'");

            RuleFor(data => data.PurchasedPortfolioOrSoldToName)
                .Must(BeBlankIfPortfolioIndicatorIsRemovePrevious)
                .WithMessage($"Account number {Context.AccountNumber} - PurchasedPortfolioOrSoldToName must be blank if PortfolioIndicator is 'RemovePrevious'");
        }

        private bool BeValidLookupValue(string portfolioIndicator)
        {
            return portfolioIndicator.ToLower() == sale.ToLower() || portfolioIndicator.ToLower() == purchase.ToLower() || portfolioIndicator.ToLower() == removePrevious.ToLower();
        }

        private bool BeBlankIfPortfolioIndicatorIsRemovePrevious(string portfolioIndicator)
        {
            return portfolioIndicator == removePrevious && string.IsNullOrEmpty(AccountPurchaseSaleInfo.PurchasedPortfolioOrSoldToName);
        }
    }
}

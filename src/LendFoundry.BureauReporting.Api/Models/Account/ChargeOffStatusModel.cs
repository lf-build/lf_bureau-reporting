﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Account
{
    public class ChargeOffStatusModel : AccountStatusModel
    {
        public string Status { get; set; }
        public DateTime RecommendedDate { get; set; }
        public DateTime InitiatedDate { get; set; }
        public DateTime CompletionDate { get; set; }
        public int ChargeOffAmount { get; set; }
    }
}

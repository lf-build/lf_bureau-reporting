﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes the base segment of Metro2
    /// </summary>
    public class BorrowerInfo
    {
        public Contact Contact { get; set; }
        public string ComplianceConditionCode { get; set; }
        public string ConsumerIndicator { get; set; }
        public string ECOACode { get; set; }
        public string GenerationCode { get; set; }
        public double HighestCredit { get; set; }
        public string InterestTypeIndicator { get; set; }
        public double OriginalChargeOffAmount { get; set; }
        public string PaymentHistory { get; set; }
        public string PaymentRating { get; set; }
        public string PortfolioType { get; set; } // can we handle this via routing?
        public double SchedulesMonthlyPayment { get; set; }
        public string SpecialComment { get; set; }
        public int TermsDuration { get; set; }
        public string TermsFrequency { get; set; }
        public string TransactionType { get; set; }
    }
}





//Borrower_FirstName
//Borrower_Surname
//Borrower_MiddleName
//Borrower_Generation
//Borrower_DateOfBirth
//Borrower_SocialSecurityNumber
//Borrower_AddressFirstLine
//Borrower_AddressIndicator
//Borrower_AddressSecondLine
//Borrower_CountryCode
//Borrower_State
//Borrower_City
//Borrower_ZipCode
//Borrower_TelephoneNumber
//Borrower_ResidenceCode
//Borrower_ComplianceConditionCode
//Borrower_ConsumerIndicator
//Borrower_ECOACode
//Borrower_GenerationCode
//Borrower_HighestCredit
//Borrower_InterestTypeIndicator
//Borrower_OriginalChargeOffAmount
//Borrower_PaymentHistory
//Borrower_PaymentRating
//Borrower_PortfolioType
//Borrower_SchedulesMonthlyPayment
//Borrower_SpecialComment
//Borrower_TermsDuration
//Borrower_TermsFrequency
//Borrower_TransactionType

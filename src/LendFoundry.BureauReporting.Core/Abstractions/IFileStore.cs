﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Abstractions
{
    public interface IFileStore
    {
        void SaveMetro2File(string text);
        void SaveReportDataFile();
        void RetrieveFilesToImport();
        void ArchiveFiles();
    }
}

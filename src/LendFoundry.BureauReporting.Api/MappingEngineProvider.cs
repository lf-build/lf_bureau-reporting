﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api
{
    public class MappingEngineProvider
    {
        public static IMappingEngine GetMappingEngine()
        {
            return new MappingEngine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Payments
{
    public class PaymentRecord
    {
        public DateTime PaymentDate { get; set; }
        public Amount PaymentAmount { get; set; }
        public Amount AmountDueAfterPayment { get; set; }
    }
}

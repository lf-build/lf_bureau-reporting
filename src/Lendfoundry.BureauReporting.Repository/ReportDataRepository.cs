﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain;

namespace Lendfoundry.BureauReporting.Repository
{
    public class ReportDataRepository : IReportDataRepository
    {
        private static List<ReportData> data;

        public ReportDataRepository()
        {
            if (data == null)
            {
                data = new List<ReportData>();
            }
        }

        // TODO: Provide implementation
        public void AddBureauReportData(ICollection<ReportData> reportData)
        {
            foreach (var record in reportData)
            {
                data.Add(record);
            }
        }

        public ICollection<ReportData> GetReportDataToBeSubmitted()
        {
            return data;
        }
    }
}

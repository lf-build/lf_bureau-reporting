﻿using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class ViewModelValidator<T> : AbstractValidator<T>
    {
        private IValidationContext Context { get; set; }
        public ViewModelValidator(T model, IValidationContext context)
        {
            Context = context;
        }
    }
}

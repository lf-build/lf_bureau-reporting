﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class PaymentStatusValidator : ViewModelValidator<PaymentStatus>
    {
        private IValidationContext Context { get; set; }

        public PaymentStatusValidator(PaymentStatus model, IValidationContext context) : base(model, context)
        {
            Context = context;

            RuleFor(data => data.MonthNumber)
                                .Must(BeValidMonthNumber)
                                .WithMessage("MonthNumber must be a value between 1 and 24.");

            RuleFor(data => data.Status)
                                .Must(BeValidPaymentStatus)
                                .WithMessage("Status has an invalid value.");
        }

        private bool BeValidMonthNumber(int monthNumber)
        {
            return monthNumber >= 1 && monthNumber <= 24;
        }

        private bool BeValidPaymentStatus(string status)
        {
            return Context.Lookups.PaymentStatuses.ContainsKey(status);
        }
    }
}

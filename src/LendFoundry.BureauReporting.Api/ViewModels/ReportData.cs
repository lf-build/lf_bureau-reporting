﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class ReportData
    {
        public BorrowerInfo BorrowerInfo { get; set; } // part of base segment
        public AccountInfo AccountInfo { get; set; } // part of base segment
        public ICollection<CoSignerInfo> CoSigners { get; set; } // j1 and j2 segments
        public CreditorInfo CreditorInfo { get; set; } // k1 segment
        public AccountPurchaseSaleInfo AccountPurchaseAndSaleInfo { get; set; } // k2 segment
        public SecondaryMarketingAgencyInfo SecondaryMarketingAgencyInfo { get; set; } // part of k3 segment
        public MortgageInfo MortgageInfo { get; set; } // part of k3 segment
        public SpecializedPaymentInfo SpecializedPaymentInfo { get; set; } // k4 segment
        public UpdatedConsumerInfo UpdatedConsumerInfo { get; set; } // l1 segment
        public BorrowerEmploymentInfo BorrowerEmploymentInfo { get; set; } // n1 segment
    }
}

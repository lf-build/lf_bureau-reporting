﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes the K4 segment of Metro2
    /// </summary>
    public class SpecializedPaymentInfo
    {
        public string SpecializedPaymentIndicator { get; set; }

        public DateTime? DeferredPaymentStartDate { get; set; }

        public DateTime? PaymentDueDate { get; set; }

        public double PaymentAmount { get; set; }
    }
}


//SpecializedPmt_Indicator
//SpecializedPmt_DeferredPmtStartDate
// SpecializedPmt_DueDate
// SpecializePmt_PmtAmt
﻿using LendFoundry.BureauReporting.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Loan
{
    public class LoanInfoModel
    {
        public string LoanNumber { get; set; }
        public int FundedLoanAmount { get; set; }
        public DateTime LoanFundedDate { get; set; } // can we rename this to LoanDate?
        public string Ownership { get; set; }
        public int MonthlyPaymentAmount { get; set; }
        public int Terms { get; set; }
        public ProductInfoModel Product { get; set; }
    }
}

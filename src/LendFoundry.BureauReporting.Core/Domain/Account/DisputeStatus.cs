﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class DisputeStatus : AccountStatus
    {
        public string Status { get; set; }
    }
}

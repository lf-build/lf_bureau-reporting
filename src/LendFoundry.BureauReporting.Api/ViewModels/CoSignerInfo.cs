﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes the J1/J2 segments of Metro2
    /// </summary>
    public class CoSignerInfo
    {
        public Contact Contact { get; set; }
        public string ECOACode { get; set; }
        public string ConsumerInformationIndicator { get; set; }
        public string TransactionType { get; set; }
    }
}

//CoSigner1_FirstName
//CoSigner1_Surname
//CoSigner1_MiddleName
//CoSigner1_Generation
//CoSigner1_DateOfBirth
//CoSigner1_SocialSecurityNumber
//CoSigner1_AddressFirstLine
//CoSigner1_AddressIndicator
//CoSigner1_AddressSecondLine
//CoSigner1_CountryCode
//CoSigner1_State
//CoSigner1_City
//CoSigner1_ZipCode
//CoSigner1_TelephoneNumber
//CoSigner1_ResidenceCode
//CoSigner1_ECOACode
//CoSigner1_ConsumerInformationIndicator
//CoSigner1_TransactionType
//CoSigner2_FirstName
//CoSigner2_Surname
//CoSigner2_MiddleName
//CoSigner2_Generation
//CoSigner2_DateOfBirth
//CoSigner2_SocialSecurityNumber
//CoSigner2_AddressFirstLine
//CoSigner2_AddressIndicator
//CoSigner2_AddressSecondLine
//CoSigner2_CountryCode
//CoSigner2_State
//CoSigner2_City
//CoSigner2_ZipCode
//CoSigner2_TelephoneNumber
//CoSigner2_ResidenceCode
//CoSigner2_ECOACode
//CoSigner2_ConsumerInformationIndicator
//CoSigner2_TransactionType

//CoSigner3_FirstName
//CoSigner3_Surname
//CoSigner3_MiddleName
//CoSigner3_Generation
//CoSigner3_DateOfBirth
//CoSigner3_SocialSecurityNumber
//CoSigner3_AddressFirstLine
//CoSigner3_AddressIndicator
//CoSigner3_AddressSecondLine
//CoSigner3_CountryCode
//CoSigner3_State
//CoSigner3_City
//CoSigner3_ZipCode
//CoSigner3_TelephoneNumber
//CoSigner3_ResidenceCode
//CoSigner3_ECOACode
//CoSigner3_ConsumerInformationIndicator
//CoSigner3_TransactionType

//CoSigner4_FirstName
//CoSigner4_Surname
//CoSigner4_MiddleName
//CoSigner4_Generation
//CoSigner4_DateOfBirth
//CoSigner4_SocialSecurityNumber
//CoSigner4_AddressFirstLine
//CoSigner4_AddressIndicator
//CoSigner4_AddressSecondLine
//CoSigner4_CountryCode
//CoSigner4_State
//CoSigner4_City
//CoSigner4_ZipCode
//CoSigner4_TelephoneNumber
//CoSigner4_ResidenceCode
//CoSigner4_ECOACode
//CoSigner4_ConsumerInformationIndicator
//CoSigner4_TransactionType

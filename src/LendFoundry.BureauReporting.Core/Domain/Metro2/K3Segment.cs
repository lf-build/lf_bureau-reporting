﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Metro2
{
    public class K3Segment : Segment
    {
        public string SegmentIdentifier { get; set; }

        public string AgencyIdentifier { get; set; }

        public string AccountNumber { get; set; }

        public string MortgageIdentificationNumber { get; set; }

        public string Accept(IMetro2Visitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}

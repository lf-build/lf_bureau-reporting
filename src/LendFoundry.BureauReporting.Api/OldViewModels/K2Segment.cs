﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K2Segment : FileSegment
    {
        public string SegmentIdentifier { get; set; }

        public string PortfolioIndicator { get; set; }

        public string PurchasedPortfolioOrSoldToName { get; set; }

        public string Reserved { get; set; }
    }
}
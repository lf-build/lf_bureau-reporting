﻿using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class SecondaryMarketingAgencyInfoValidator : ViewModelValidator<SecondaryMarketingAgencyInfo>
    {
        private SecondaryMarketingAgencyInfo SecondaryMarketingAgencyInfo { get; set; }
        private IValidationContext Context { get; set; }

        public SecondaryMarketingAgencyInfoValidator(SecondaryMarketingAgencyInfo agencyInfo, IValidationContext context) : base(agencyInfo, context)
        {
            SecondaryMarketingAgencyInfo = agencyInfo;
            Context = context;

            RuleFor(data => data.AgencyIdentifier)
                                .NotEmpty()
                                    .WithMessage("AgencyIdentifier is required.")
                                .Must(BeValidAgencyIdentifier)
                                    .WithMessage("AgencyIdentifier has an invalid value.");
        }

        private bool BeValidAgencyIdentifier(string identifier)
        {
            return Context.Lookups.AgencyIdentifiers.ContainsKey(identifier);
        }

        public void ValidateAgencyAssignedAccountNumber()
        {
            RuleFor(data => data.AgencyAssignedAccountNumber)
                                .Must(BeEmptyIfAgencyIdentifierIsUnspecified)
                                .WithMessage("AccountNumber must be empty if agency has a value of NotApplicable.");
                            
        }

        private bool BeEmptyIfAgencyIdentifierIsUnspecified(string accountNumber)
        {
            return SecondaryMarketingAgencyInfo.AgencyIdentifier == "NotApplicable" && string.IsNullOrEmpty(accountNumber);
        }
    }
}

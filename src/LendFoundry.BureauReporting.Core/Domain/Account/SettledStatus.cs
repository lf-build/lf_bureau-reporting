﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class SettledStatus : AccountStatus
    {
        public string Status { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public int LastPaymentAmount { get; set; }
    }
}

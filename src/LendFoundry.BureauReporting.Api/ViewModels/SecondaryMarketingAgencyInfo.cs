﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes part of K3 segment info of Metro2, but is exposed separately. The other details are in MortgageInfo.
    /// </summary>
    public class SecondaryMarketingAgencyInfo
    {
        public string AgencyIdentifier { get; set; }

        public string AgencyAssignedAccountNumber { get; set; }
    }
}


//SecondaryMktgAgency_AgencyIdentifier
//SecondaryMktgAgency_AgencyAssignedAccountNumber
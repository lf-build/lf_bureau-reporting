﻿using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class AccountInfoValidator : ViewModelValidator<AccountInfo>
    {
        private AccountInfo AccountInfo { get; set; }
        private IValidationContext Context { get; set; }
        public AccountInfoValidator(AccountInfo accountInfo, IValidationContext context) : base(accountInfo, context)
        {
            AccountInfo = accountInfo;
            Context = context;

            ValidateAccountNumber();
            ValidateAccountStatus();
            ValidateAccountType();
            ValidatePortfolio();
            ValidateAmountPastDue();
            ValidateDateOpened();
            ValidatePaymentHistory();
        }

        /// <summary>
        /// Accepts account numbers that contain numbers and or letters only.
        /// Checks for uniqueness before the account number can be added (no duplicates allowed) 
        /// Checks for the Consumers Social Security Number in the account number 
        /// Checks for embedded spaces
        /// Account number must be 30 characters or less
        /// The account number cannot be all zeros
        /// The account number cannot be blank
        /// Includes other compliance checking measures
        /// </summary>
        /// <returns></returns>
        private void ValidateAccountNumber()
        {
            RuleFor(data => data.AccountNumber)
                                .NotEmpty()
                                    .WithMessage("AccountNumber is required.")
                                .Length(1, 30)
                                    .WithMessage("AccountNumber must be between 1 and 30 characters long.")
                                .Must(NotContainOnlyZeroes)
                                    .WithMessage("AccountNumber must not contain only zeroes.")
                                .Must(NotContainEmbeddedSpaces)
                                    .WithMessage("AccountNumber must not contain spaces.")
                                .Matches("^[a-zA-Z0-9]*$")
                                    .WithMessage("AccountNumber must contain only numbers and letters.");
        }

        private void ValidateAccountStatus()
        {
            RuleFor(data => data.AccountStatus)
                                .NotEmpty()
                                    .WithMessage("AccountStatus is required.")
                                .Must(BeValidStatusLookupValue)
                                    .WithMessage("AccountStatus has an invalid value.");
        }

        private void ValidateAccountType()
        {
            RuleFor(data => data.AccountType)
                                .NotEmpty()
                                    .WithMessage("AccountType is required.")
                                .Must(BeValidAccountTypeLookupValue)
                                    .WithMessage("AccountType has an invalid value.")
                                .Must(BeValidForPortfolio)
                                    .WithMessage("The specified AccountType value is not valid for this portfolio.");
        }

        private void ValidatePortfolio()
        {
            RuleFor(data => data.Portfolio)
                                .NotEmpty()
                                    .WithMessage("Portfolio was not specified.")
                                .Must(BeValidPortfolio)
                                    .WithMessage("Portfolio was not recognized");
        }


        private bool BeValidPortfolio(string portfolio)
        {
            if(!string.IsNullOrEmpty(portfolio))
            {
                return portfolio == "LineOfCredit" ||
                       portfolio == "Installment" ||
                       portfolio == "Open" ||
                       portfolio == "Revolving";
            }

            return false;
        }

        private void ValidateInstallmentAmount()
        {
            RuleFor(data => data.ActualPaymentAmount)
                                .GreaterThan(0)
                                .WithMessage("ActualPaymentAmount must be greater than zero.");
        }

        private void ValidateAmountPastDue()
        {
            // If the account status is current (Status code 11), this field should be 0.
            RuleFor(data => data.AmountPastDue)
                                .Must(BeZeroIfAccountStatusIsCurrent)
                                .WithMessage("AmountPastDue must be zero if account status is current.");
        }

        private void ValidateDateOpened()
        {
            var tenantTime = Context.TenantTime;
            RuleFor(data => tenantTime.FromDate(data.DateOpened))
                                                    .LessThan(tenantTime.Now)
                                                    .WithMessage("DateOpened must not be greater than current date.");

        }

        private void ValidateDateClosed()
        {
            // Format for packed date is 0MMDDYYYYs — where s is the sign. Format is MMDDYYYY for character date. 
            // If not applicable, zero fill.

            var tenantTime = Context.TenantTime;

            // This is not in the documentation, but it'd be safe to assume that DateClosed cannot be greater than DateOpened
            RuleFor(data => tenantTime.FromDate(data.DateClosed))
                                        .GreaterThan(AccountInfo.DateOpened)
                                        .WithMessage("DateClosed must not be lesser than DateOpened.");
        }

        private void ValidateDateOfAccountInformation()
        {
            var tenantTime = Context.TenantTime;
            RuleFor(data => tenantTime.FromDate(data.DateOpened))
                                                    .LessThan(tenantTime.Now)
                                                    .WithMessage("DateOfAccountInformation must not be greater than current date.");

            // TODO: This date should not change after the account has been reported as paid (Account Status 13, 61–65) or transferred (Account Status 05). 
            // TODO: Implement the above validation after persistence has been implemented.
        }

        private void ValidateDateOfFirstDelinquency()
        {
            //This date must be more than 30 days after the Date Opened
            //The account must be at least 30 days late before reporting it as delinquent
            //Date format is MM/DD/YYYY

            if (AccountInfo.DateOfFirstDelinquency != null)
            {
                RuleFor(data => data.DateOfFirstDelinquency)
                                    .Must(BeGreaterThan30DaysAfterAccountOpenDate)
                                    .WithMessage("DateOfFirstDelinquency must be more than 30 days after AccountOpeningDate.");
            }

            // TODO: The account must be at least 30 days late before reporting it as delinquent 
            // 1. Should we implement the above validation?
            // 2. If so, how do we know if the account is at least 30 days late? Can 23 check AccountStatus? Or PaymentHistory? 
            //    But those contain values like "Reposession", "Chargeoff" etc. that don't say how much late the account is. 
            //    Need to get this clarified.

        }

        private bool BeGreaterThan30DaysAfterAccountOpenDate(DateTime? dateOfFirstDelinquency)
        {
            return ((DateTime)dateOfFirstDelinquency - Context.AccountOpeningDate).TotalDays > 30;
        }

        private void ValidateDateOfLastPayment()
        {
            // No validations, per documentation.
        }

        private bool BeZeroIfAccountStatusIsCurrent(double amountPastDue)
        {
            return Context.AccountStatusName == "Current" && amountPastDue == 0;
        }

        private bool BeValidStatusLookupValue(string arg)
        {
            return Context.Lookups.AccountStatusCodes.ContainsKey(arg);
        }

        private bool BeValidAccountTypeLookupValue(string arg)
        {
            return Context.Lookups.AccountTypePortfolioMap.ContainsKey(arg);
        }

        private bool BeValidForPortfolio(string accountType)
        {
            var record = Context.Lookups.AccountTypePortfolioMap[accountType];

            // TODO: Refactor this
            switch(Context.Portfolio)
            {
                case "LineOfCredit":
                    return record.Item2;
                case "Installment":
                    return record.Item3;
                case "Mortgage":
                    return record.Item4;
                case "Open":
                    return record.Item5;
                case "Revolving":
                    return record.Item6;
                default:
                    throw new Exception($"Unrecognized portfolio : {Context.Portfolio}");
            }
        }

        private void ValidatePaymentHistory()
        {
            RuleFor(data => data.PaymentHistory)
                                .NotEmpty()
                                .WithMessage("PaymentHistory is required.");

            if (AccountInfo.PaymentHistory != null)
            {
                foreach (var status in AccountInfo.PaymentHistory)
                {
                    // doing this in a loop instead of just calling SetValidator on the whole collection
                    // because of the constructor signature of the validator.
                    RuleFor(data => status).SetValidator(new PaymentStatusValidator(status, Context));
                }
            }
        }

        private bool NotContainOnlyZeroes(string arg)
        {
            return !(arg.Count(c => c == '0') == arg.Length);
        }

        private bool NotContainEmbeddedSpaces(string arg)
        {
            return !arg.Contains(" ");
        }
    }
}

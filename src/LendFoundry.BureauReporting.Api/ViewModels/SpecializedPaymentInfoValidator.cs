﻿using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class SpecializedPaymentInfoValidator : ViewModelValidator<SpecializedPaymentInfo>
    {
        private SpecializedPaymentInfo SpecializedPaymentInfo { get; }
        private IValidationContext Context { get; }
        public SpecializedPaymentInfoValidator(SpecializedPaymentInfo specializedPaymentInfo, IValidationContext context) : base(specializedPaymentInfo, context)
        {
            SpecializedPaymentInfo = specializedPaymentInfo;
            Context = context;

            RuleFor(data => data.SpecializedPaymentIndicator)
                                .Must(BeValidSpecializedPaymentIndicator)
                                .WithMessage("SpecializedPaymentIndicator has an invalid value.");

            ValidateDeferredPaymentStartDate();
            ValidatePaymentDueDate();
        }

        private bool BeValidSpecializedPaymentIndicator(string paymentIndicator)
        {
            return Context.Lookups.SpecializedPaymentIndicators.ContainsKey(paymentIndicator);
        }

        private void ValidateDeferredPaymentStartDate()
        {
            // This date should be reported when the terms frequency indicates deferred
            RuleFor(data => data.DeferredPaymentStartDate)
                                .NotEmpty()
                                .When(TermsFrequencyIndicatesDeferred)
                                .WithMessage("DeferredPaymentStartDate should be specified when TermsFrequencyCode is 'Deferred'.");

        }

        private bool TermsFrequencyIndicatesDeferred(SpecializedPaymentInfo specializedPaymentInfo)
        {
            return Context.TermsFrequency == "Deferred";
        }

        private void ValidatePaymentDueDate()
        {
            // No validations, per documentation
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Payments
{
    public class PaymentScheduleModel
    {
        public DateTime ScheduledDate { get; set; }
        public AmountModel ScheduledAmountDue { get; set; }
        public AmountModel OverdueAmount { get; set; }
    }
}

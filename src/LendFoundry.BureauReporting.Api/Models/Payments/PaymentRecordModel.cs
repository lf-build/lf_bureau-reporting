﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Payments
{
    public class PaymentRecordModel
    {
        public DateTime PaymentDate { get; set; }
        public AmountModel PaymentAmount { get; set; }
        public AmountModel AmountDueAfterPayment { get; set; }
    }
}

﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using LendFoundry.BureauReporting.Core.Domain.Borrower;
using LendFoundry.BureauReporting.Core.Domain.Loan;
using LendFoundry.BureauReporting.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain
{
    public class ReportData
    {
        public AccountInfo AccountInfo { get; set; }
        public ICollection<BorrowerInfo> BorrowerInfo { get; set; }
        public LoanInfo LoanInfo { get; set; }
        //public ProductInfo ProductInfo { get; set; }
        public ICollection<PaymentRecord> PaymentHistory { get; set; }
    }
}

﻿using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Transforms
{
    public class Metro2TranslationManager
    {
        private ReportData ReportData { get; }

        public Metro2TranslationManager(ReportData reportData)
        {
            ReportData = reportData;
        }

        public Metro2Data CreateMetro2()
        {
            var translator = new Metro2Translator(ReportData);
            var metro2Data = translator
                                .CreateHeaderRecordSegment()
                                .CreateBaseSegment()
                                .CreateTrailerRecordSegment()
                                .CreateMetro2Data();

            return metro2Data;
        }
    }
}

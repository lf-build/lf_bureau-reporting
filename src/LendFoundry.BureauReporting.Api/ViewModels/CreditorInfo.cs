﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes the K1 segment of Metro2
    /// </summary>
    public class CreditorInfo
    {
        public string CreditorClassification { get; set; }
        public string OriginalCreditorName { get; set; }
        public string CreditorAccountNumber { get; set; } // this does not get reported to credit bureaus. see documentaion.
    }
}


// Creditor_Classification
// Creditor_OriginalName
// Creditor_AccountNumber
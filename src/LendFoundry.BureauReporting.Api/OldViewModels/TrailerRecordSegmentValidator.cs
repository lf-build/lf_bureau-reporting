﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class TrailerRecordSegmentValidator : AbstractValidator<TrailerRecordSegment>
    {
        private const string SegmentName = "Trailer Record Segment";

        public TrailerRecordSegmentValidator()
        {
            RuleFor(segment => segment.RecordDescriptorWord).NotEmpty().WithMessage($"{SegmentName} - RecordDescriptorWord cannot be empty.");
            RuleFor(segment => segment.RecordIdentifier).NotEmpty().WithMessage($"{SegmentName} - RecordIdentifier cannot be empty.");
            RuleFor(segment => segment.TotalBaseRecords).NotEmpty().WithMessage($"{SegmentName} - TotalBaseRecords cannot be empty.");
            RuleFor(segment => segment.TotalAssociatedConsumerSegmentsJ1).NotEmpty().WithMessage($"{SegmentName} - TotalAssociatedConsumerSegmentsJ1 cannot be empty.");
            RuleFor(segment => segment.TotalAssociatedConsumerSegmentsJ2).NotEmpty().WithMessage($"{SegmentName} - TotalAssociatedConsumerSegmentsJ2 cannot be empty.");
            RuleFor(segment => segment.BlockCount).NotEmpty().WithMessage($"{SegmentName} - BlockCount cannot be empty.");
        }
    }
}

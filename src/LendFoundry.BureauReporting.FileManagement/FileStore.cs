﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.FileManagement
{
    public class FileStore : IFileStore
    {
        public void SaveReportDataFile()
        {
            throw new NotImplementedException();
        }

        public void RetrieveFilesToImport()
        {
            throw new NotImplementedException();
        }

        public void ArchiveFiles()
        {
            throw new NotImplementedException();
        }

        public void SaveMetro2File(string text)
        {
            // TODO: Figure out a proper file storage strategy
            File.AppendAllText(@"C:\temp\bureaureporting\Metro2.txt", text);
        }
    }
}

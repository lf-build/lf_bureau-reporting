﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class J2Segment : FileSegment
    {
        public string SegmentIdentifier { get; set; }

        public string ConsumerTransactionType { get; set; }

        public string Surname { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string GenerationCode { get; set; }

        public string SocialSecurityNumber { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string TelephoneNumber { get; set; }

        public string ECOACode { get; set; }

        public string ConsumerInformationIndicator { get; set; }

        public string CountryCode { get; set; }

        public string FirstLineOfAddress { get; set; }

        public string SecondLineOfAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string AddressIndicator { get; set; }

        public string ResidenceCode { get; set; }

        public string Reserved { get; set; }
    }
}











﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain;
using AutoMapper;
using LendFoundry.BureauReporting.Api.Models;

namespace LendFoundry.BureauReporting.Api
{

    public interface IMappingEngine
    {
        void CreateMappings();
    }
}

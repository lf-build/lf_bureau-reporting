﻿using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Unpacked
{
    public class TrailerRecordFormatter
    {
        private Metro2Data Metro2Data { get; }

        public TrailerRecordFormatter(Metro2Data metro2Data)
        {
            Metro2Data = metro2Data;
        }

        private string GetRecordDescriptorWord()
        {
            return " ".PadLeft(4); // TODO: Get the record descriptor word (if applicable)
        }

        private int CalculateTotalBaseSegments()
        {
            return 1; // TODO: Calculate the number of base segments being reported
        }

        private int CalculateTotalJ1Segments()
        {
            return 0; // TODO: Calculate the number of J1 segments being reported
        }

        private int CalculateTotalJ2Segments()
        {
            return 0; // TODO: Calculate the number of J2 segments being reported
        }

        public string GenerateSegment()
        {
            var sb = new StringBuilder();

            // Record Descriptor Word(RDW) ............................ 1-4
            sb.Append(GetRecordDescriptorWord());

            // Record Identifier ...................................... 5-11
            sb.Append("TRAILER"); // fixed value of TRAILER (6 letters)

            // Total Base Records .................................... 12-20
            sb.Append(CalculateTotalBaseSegments());

            // Reserved .............................................. 21-29
            sb.Append(" ".PadLeft(9)); // 9 spaces reserved, blank fill

            // Reserved .............................................. 30-38
            sb.Append(" ".PadLeft(9)); // 9 spaces reserved, blank fill

            // Total Associated Consumer Segments(J1) ................ 39-47
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of J1 segments and pad zeroes to left if required

            // Total Associated Consumer Segments(J2) ................ 48-56
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of J2 segments and pad zeroes to left if required

            // Block Count ........................................... 57-65
            sb.Append("0".PadLeft(9)); // TODO: Calculate block count and pad with zeroes if required

            // Total of Status Code DA ............................... 66-74
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of DA status codes and pad zeroes to left if required

            // Total of Status Code 05 ............................... 75-83
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of 05 status codes and pad zeroes to left if required

            // Total of Status Code 11 ............................... 84-92
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 13 ............................... 93-101
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 61 .............................. 102-110
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 62 .............................. 111-119
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 63 .............................. 120-128
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 64 .............................. 129-137
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 65 .............................. 138-146
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 71 .............................. 147-155
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 78 .............................. 156-164
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 80 .............................. 165-173
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 82 .............................. 174-182
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 83 .............................. 183-191
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 84 .............................. 192-200
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 88 .............................. 201-209
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 89 .............................. 210-218
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 93 .............................. 219-227
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 94 .............................. 228-236
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 95 .............................. 237-245
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 96 .............................. 246-254
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of Status Code 97 .............................. 255-263
            sb.Append("0".PadLeft(9)); // TODO: Calculate the number of status codes and pad zeroes to left if required

            // Total of ECOA Code Z(All Segments) ................... 264-272
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Employment Segments ............................ 273-281
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Original Creditor Segments ..................... 282-290
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Purchased Portfolio/Sold To Segments ........... 291-299
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Mortgage Information Segments .................. 300-308
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Specialized Payment Information Segments ....... 309-317
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Change Segments ................................ 318-326
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Social Security Numbers(All Segments) .......... 327-335
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Social Security Numbers(Base Segments) ......... 336-344
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Social Security Numbers(J1 Segments) ........... 345-353
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Social Security Numbers(J2 Segments) ........... 354-362
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Dates of Birth(All Segments) ................... 363-371
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Dates of Birth(Base Segments) .................. 372-380
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Dates of Birth(J1 Segments) .................... 381-389
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Dates of Birth(J2 Segments) .................... 390-398
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Total Telephone Numbers(All Segments) ................ 399-407
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            // Reserved ............................................. 408-426
            sb.Append("0".PadLeft(9)); // TODO: Calculate the total and pad zeroes to left if required

            return sb.ToString();
        }
    }
}
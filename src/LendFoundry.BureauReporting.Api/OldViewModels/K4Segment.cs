﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K4Segment : FileSegment
    {
        public string SegmentIdentifier { get; set; }

        public string SpecializedPaymentIndicator { get; set; }

        public DateTime? DeferredPaymentStartDate { get; set; }

        public DateTime? PaymentDueDate { get; set; }

        public double? PaymentAmount { get; set; }

        public string Reserved { get; set; }

    }
}

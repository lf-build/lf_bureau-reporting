﻿using LendFoundry.BureauReporting.Core.Abstractions;
using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Transforms
{
    public class Metro2Translator : IMetro2Translator
    {
        private ReportData ReportData { get; }

        private HeaderRecordSegment HeaderRecordSegment;
        private BaseSegment BaseSegment;
        private J1Segment J1Segment;
        private J2Segment J2Segment;
        private K1Segment K1Segment;
        private K2Segment K2Segment;
        private K3Segment K3Segment;
        private K4Segment K4Segment;
        private L1Segment L1Segment;
        private N1Segment N1Segment;
        private TrailerRecordSegment TrailerRecordSegment;

        public Metro2Translator(ReportData reportData)
        {
            ReportData = reportData;
        }

        public IMetro2Translator CreateHeaderRecordSegment()
        {
            var segmentCreator = new HeaderRecordDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.HeaderRecordSegment = (HeaderRecordSegment)segment;
            return this;
        }
        public IMetro2Translator CreateBaseSegment()
        {
            var segmentCreator = new BaseSegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.BaseSegment = (BaseSegment)segment;
            return this;
        }

        public IMetro2Translator CreateJ1Segment()
        {
            var segmentCreator = new J1SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.J1Segment = (J1Segment)segment;
            return this;

        }

        public IMetro2Translator CreateJ2Segment()
        {
            var segmentCreator = new J2SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.J2Segment = (J2Segment)segment;
            return this;
        }

        public IMetro2Translator CreateK1Segment()
        {
            var segmentCreator = new K1SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.K1Segment = (K1Segment)segment;
            return this;
        }

        public IMetro2Translator CreateK2Segment()
        {
            var segmentCreator = new K2SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.K2Segment = (K2Segment)segment;
            return this;
        }

        public IMetro2Translator CreateK3Segment()
        {
            var segmentCreator = new K3SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.K3Segment = (K3Segment)segment;
            return this;
        }

        public IMetro2Translator CreateK4Segment()
        {
            var segmentCreator = new K4SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.K4Segment = (K4Segment)segment;
            return this;
        }

        public IMetro2Translator CreateL1Segment()
        {
            var segmentCreator = new L1SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.L1Segment = (L1Segment)segment;
            return this;
        }

        public IMetro2Translator CreateN1Segment()
        {
            var segmentCreator = new N1SegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.N1Segment = (N1Segment)segment;
            return this;
        }

        public IMetro2Translator CreateTrailerRecordSegment()
        {
            var segmentCreator = new TrailerRecordSegmentDataCreator(ReportData);
            var segment = segmentCreator.GetSegment();
            this.TrailerRecordSegment = (TrailerRecordSegment)segment;
            return this;
        }

        public Metro2Data CreateMetro2Data()
        {
            var metro2Data = new Metro2Data
            {
                HeaderRecordSegment = this.HeaderRecordSegment,
                BaseSegment = this.BaseSegment,
                J1Segment = this.J1Segment,
                J2Segment = this.J2Segment,
                K1Segment = this.K1Segment,
                K2Segment = this.K2Segment,
                K3Segment = this.K3Segment,
                K4Segment = this.K4Segment,
                L1Segment = this.L1Segment,
                N1Segment = this.N1Segment,
                TrailerRecordSegment = this.TrailerRecordSegment
            };

            return metro2Data;
        }
    }
}

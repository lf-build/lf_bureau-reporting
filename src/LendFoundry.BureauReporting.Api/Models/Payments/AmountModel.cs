﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Payments
{
    public class AmountModel
    {
        public int Principal { get; set; }
        public int Interest { get; set; }
        public int Fees { get; set; }
        public int Charges { get; set; }
    }
}
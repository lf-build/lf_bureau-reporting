﻿using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Unpacked
{
    public class HeaderRecordFormatter
    {
        private Metro2Data Metro2Data { get; }

        private string GetRecordDescriptor()
        {
            return "HEAD"; // TODO: Compute the descriptor value
        }

        public HeaderRecordFormatter(Metro2Data metro2Data)
        {
            Metro2Data = metro2Data;
        }

        public string GenerateSegment()
        {
            var headerData = Metro2Data.HeaderRecordSegment;
            var sb = new StringBuilder();

            // Record Descriptor Word(RDW)............ 1-4
            sb.Append(GetRecordDescriptor());

            // Record Identifier ..................... 5-10
            sb.Append("HEADER"); // constant of header

            // Cycle Number ......................... 11-12
            sb.Append("  "); // TODO: Field is required if reporting by cycles, otherwise blank fill.

            // Innovis Program Identifier ........... 13-22
            sb.Append(headerData.InnovisProgramIdentifier); // TODO: Get unique identification number assigned by this consumer reporting agency

            // Equifax Program Identifier ........... 23-32
            sb.Append(headerData.EquifaxProgramIdentifier); // TODO: Get unique identification number assigned by this consumer reporting agency

            // Experian Program Identifier .......... 33-37
            sb.Append(headerData.ExperianProgramIdentifier); // TODO: Get unique identification number assigned by this consumer reporting agency

            // TransUnion Program Identifier ........ 38-47
            sb.Append(headerData.TransUnionProgramIdentifier); // TODO: Get unique identification number assigned by this consumer reporting agency

            // Activity Date ........................ 48-55
            sb.Append(headerData.ActivityDate);

            // Date Created ......................... 56-63
            sb.Append(headerData.DateCreated);

            // Program Date ......................... 64-71
            sb.Append(headerData.ProgramDate);

            // Program Revision Date ................ 72-79
            sb.Append(headerData.ProgramRevisionDate);

            // Reporter Name ........................ 80-119
            sb.Append(headerData.ReporterName);

            // Reporter Address .................... 120-215
            sb.Append(headerData.ReporterAddress);

            // Reporter Telephone Number ........... 216-225
            sb.Append(headerData.ReporterTelephoneNumber);

            // Software Vendor Name ................ 226-265
            sb.Append(headerData.SoftwareVendorName);

            // Software Version Number ............. 266-270
            sb.Append(headerData.SoftwareVersionNumber);

            // Reserved ............................ 271-426
            sb.Append(" ".PadLeft(156)); // 156 characters reserved, blank filled.

            return sb.ToString();
        }
    }
}

﻿using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Abstractions
{
    public interface IMetro2Visitor
    {
        string Visit(HeaderRecordSegment segment);
        string Visit(BaseSegment segment);
        string Visit(J1Segment segment);
        string Visit(J2Segment segment);
        string Visit(K1Segment segment);
        string Visit(K2Segment segment);
        string Visit(K3Segment segment);
        string Visit(K4Segment segment);
        string Visit(L1Segment segment);
        string Visit(N1Segment segment);
        string Visit(TrailerRecordSegment segment);
    }
}

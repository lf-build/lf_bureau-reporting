﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K2SegmentValidator : AbstractValidator<K2Segment>
    {
        private const string SegmentName = "K2 Segment";
        public K2SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmentIdentifier cannot be empty.");
            RuleFor(segment => segment.PortfolioIndicator).NotEmpty().WithMessage($"{SegmentName} - PortfolioIndicator cannot be empty.");
            RuleFor(segment => segment.PurchasedPortfolioOrSoldToName).NotEmpty().WithMessage($"{SegmentName} - PurchasedPortfolioOrSoldToName cannot be empty.");
        }
    }
}

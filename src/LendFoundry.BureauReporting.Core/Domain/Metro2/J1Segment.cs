﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Metro2
{
    public class J1Segment : Segment
    {
        public string SegmentIdentifier { get; set; }
        public string ConsumerTransactionType { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string GenerationCode { get; set; }
        public string SocialSecurityNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string TelephoneNumber { get; set; }
        public string ECOACode { get; set; }
        public string ConsumerInformationIndicator { get; set; }
        public string Reserved { get; set; }

        public string Accept(IMetro2Visitor visitor)
        {
            return visitor.Visit(this);
        }

    }
}

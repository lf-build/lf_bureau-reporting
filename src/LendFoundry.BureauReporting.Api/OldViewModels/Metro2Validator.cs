﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class Metro2Validator : AbstractValidator<Metro2>
    {
        public Metro2Validator()
        {
            // first check to ensure that no required constituent part of the incoming Metro2 data is null
            RuleFor(segment => segment.HeaderRecordSegment).NotNull();
            RuleFor(segment => segment.BaseSegment).NotNull();
            RuleFor(segment => segment.J1Segment).NotNull();
            RuleFor(segment => segment.J2Segment).NotNull();
            RuleFor(segment => segment.K1Segment).NotNull();
            RuleFor(segment => segment.K2Segment).NotNull();
            RuleFor(segment => segment.K3Segment).NotNull();
            RuleFor(segment => segment.K4Segment).NotNull();
            RuleFor(segment => segment.L1Segment).NotNull();
            RuleFor(segment => segment.N1Segment).NotNull();
            RuleFor(segment => segment.TrailerRecorSegment).NotNull();


            // assign validators to the parts that constitute Metro2
            this.RuleFor(m => m.HeaderRecordSegment).SetValidator(new HeaderRecordSegmentValidator());
            this.RuleFor(m => m.BaseSegment).SetValidator(new BaseSegmentValidator());
            this.RuleFor(m => m.J1Segment).SetValidator(new J1SegmentValidator());
            this.RuleFor(m => m.J2Segment).SetValidator(new J2SegmentValidator());
            this.RuleFor(m => m.K1Segment).SetValidator(new K1SegmentValidator());
            this.RuleFor(m => m.K2Segment).SetValidator(new K2SegmentValidator());
            this.RuleFor(m => m.K3Segment).SetValidator(new K3SegmentValidator());
            this.RuleFor(m => m.K4Segment).SetValidator(new K4SegmentValidator());
            this.RuleFor(m => m.L1Segment).SetValidator(new L1SegmentValidator());
            this.RuleFor(m => m.N1Segment).SetValidator(new N1SegmentValidator());
            this.RuleFor(m => m.TrailerRecorSegment).SetValidator(new TrailerRecordSegmentValidator());
        }
    }
}

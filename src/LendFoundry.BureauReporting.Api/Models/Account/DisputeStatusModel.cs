﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Account
{
    public class DisputeStatusModel : AccountStatusModel
    {
        public string Status { get; set; }
    }
}

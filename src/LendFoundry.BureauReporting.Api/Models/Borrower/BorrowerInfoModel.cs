﻿using LendFoundry.BureauReporting.Api.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Borrower
{
    public class BorrowerInfoModel
    {
        public PersonModel Borrower { get; set; }
        public AddressModel Address { get; set; }
        public BorrowerStatusModel Status { get; set; }
        public string PositionInLoan { get; set; }
        public string Role { get; set; } // primary, co-signer etc, maker, co-maker, guarantor etc.
    }
}

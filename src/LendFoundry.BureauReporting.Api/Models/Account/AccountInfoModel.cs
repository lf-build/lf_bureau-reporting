﻿using LendFoundry.BureauReporting.Api.Models.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Account
{
    public class AccountInfoModel
    {
        #region Newly Added Properties
        public string AccountNumber { get; set; }
        public string InterestType { get; set; } // to be assigned to the InterestTypeIndicator in base segment
        public DateTime DateOfAccountInformation { get; set; }

        public double ActualPaymentAmount { get; set; }

        public double CreditLimit { get; set; }

        #endregion Newly Added Properties


        public AccountStatusModel AccountStatus { get; set; }
        public string PaymentStatus { get; set; } // payment status is different from account status
        public AmountModel TotalAmountDue { get; set; }
    }
}
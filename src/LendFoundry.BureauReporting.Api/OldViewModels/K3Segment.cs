﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K3Segment : FileSegment
    {
        public string SegmentIdentifier { get; set; }

        public string AgencyIdentifier { get; set; }

        public string AccountNumber { get; set; }

        public string MortgageIdentificationNumber { get; set; }


    }
}

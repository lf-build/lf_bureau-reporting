﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Metro2
{
    public class L1Segment : Segment
    {
        public string SegmentIdentifier { get; set; }

        public string ChangeIndicator { get; set; }

        public string NewConsumerAccountNumber { get; set; }

        public string NewIdentificationNumber { get; set; }

        public string Reserved { get; set; }

        public string Accept(IMetro2Visitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}

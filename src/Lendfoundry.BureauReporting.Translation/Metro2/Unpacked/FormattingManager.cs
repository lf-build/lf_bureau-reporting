﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System.Text;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Unpacked
{
    public class FormattingManager : IMetro2Visitor
    {
        private Metro2Data Metro2Data { get; }

        public FormattingManager(Metro2Data metro2Data)
        {
            Metro2Data = metro2Data;
        }

        public string GetFormattedString()
        {
            var sb = new StringBuilder();

            var header = Metro2Data.HeaderRecordSegment.Accept(this);
            sb.Append(header);
            sb.Append(Environment.NewLine);

            var baseSegment = Metro2Data.BaseSegment.Accept(this);
            sb.Append(baseSegment);
            sb.Append(Environment.NewLine);

            var trailer = Metro2Data.TrailerRecordSegment.Accept(this);
            sb.Append(trailer);

            //var header = Visit(Metro2Data.HeaderRecordSegment);
            //sb.Append(header);

            //var baseSegment = Visit(Metro2Data.BaseSegment);
            //sb.Append(baseSegment);

            //var trailer = Visit(Metro2Data.TrailerRecordSegment);
            //sb.Append(trailer);

            return sb.ToString();
        }


        public string Visit(HeaderRecordSegment segment)
        {
            var formatter = new HeaderRecordFormatter(Metro2Data);
            return formatter.GenerateSegment();
        }

        public string Visit(BaseSegment segment)
        {
            var formatter = new BaseSegmentFormatter(Metro2Data);
            return formatter.GenerateSegment();
        }

        public string Visit(J1Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(J2Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(K1Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(K2Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(K3Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(K4Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(L1Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(N1Segment segment)
        {
            throw new NotImplementedException();
        }

        public string Visit(TrailerRecordSegment segment)
        {
            var formatter = new TrailerRecordFormatter(Metro2Data);
            return formatter.GenerateSegment();
        }
    }
}

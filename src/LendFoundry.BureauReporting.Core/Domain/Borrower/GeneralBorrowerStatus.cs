﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Borrower
{
    public class GeneralBorrowerStatus : BorrowerStatus
    {
        public string Status { get; set; }
    }
}

﻿using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Transforms
{
    public interface ISegmentDataCreator
    {
        Segment GetSegment();
    }
}

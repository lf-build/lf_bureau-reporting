﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using LendFoundry.BureauReporting.Core.Domain.Account;
using LendFoundry.BureauReporting.Core.Domain;

namespace Lendfoundry.BureauReporting.Translation.Metro2
{
    public class AccountInfoTranslator
    {
        private Metro2Data Metro2Data;
        private AccountInfo AccountInfo;

        public AccountInfoTranslator(Metro2Data metro2Data)
        {
            Metro2Data = metro2Data;
        }

        public void TranslateElement(AccountInfo accountInfo)
        {
            AccountInfo = accountInfo;

            var baseSegment = Metro2Data.BaseSegment;

            baseSegment.ConsumerAccountNumber = accountInfo.AccountNumber;
            baseSegment.AccountStatus = GetAccountStatusCode();
            baseSegment.DateofFirstDelinquency = CalculateDateOfFirstDelinquency();
            baseSegment.AccountType = GetAccountType();
            //baseSegment.ECOACode = GetEcoaCode(); // Part of borrower info
            baseSegment.HighestCredit = GetHighestCredit(); // Should this be computed?
            baseSegment.InterestTypeIndicator = GetInterestTypeIndicator();
            baseSegment.OriginalChargeoffAmount = GetOriginalChargeOffAmount();
            baseSegment.PortfolioType = GetPortfolioType();
            baseSegment.SpecialComment = GetSpecialComment();
            baseSegment.TermsDuration = GetTermsDuration();
            baseSegment.TermsFrequency = GetTermsFrequency();
            baseSegment.TransactionType = GetTransactionType();
        }

        private string GetAccountType()
        {
            throw new NotImplementedException();
        }

        private string GetAccountStatusCode()
        {
            var status = string.Empty;

            if (AccountInfo.AccountStatus is GeneralAccountStatus)
            {
                var accountStatus = (GeneralAccountStatus)AccountInfo.AccountStatus;
                status = accountStatus.Status;
            }
            else if (AccountInfo.AccountStatus is SettledStatus)
            {
                var accountStatus = (SettledStatus)AccountInfo.AccountStatus;
                status = Metro2Data.BaseSegment.AccountStatus;
            }
            else if (AccountInfo.AccountStatus is DelinquencyStatus)
            {
                var accountStatus = (DelinquencyStatus)AccountInfo.AccountStatus;
                throw new NotImplementedException("Delinquency status - to be implemented.");
            }
            else if (AccountInfo.AccountStatus is DisputeStatus)
            {
                var accountStatus = (DisputeStatus)AccountInfo.AccountStatus;
                throw new NotImplementedException("Dispute status - to be implemented.");
            }

            return status;
            
        }

        private DateTime CalculateDateOfFirstDelinquency()
        {
            throw new NotImplementedException();
        }

        private string GetComplianceConditionCode()
        {
            throw new NotImplementedException();
        }

        private string GetEcoaCode()
        {
            throw new NotImplementedException();
        }

        private string GetConsumerIndicator()
        {
            throw new NotImplementedException();
        }

        private int GetCreditLimit()
        {
            throw new NotImplementedException();
        }

        private int GetCurrentBalance()
        {
            throw new NotImplementedException();
        }

        private DateTime GetClosureDate()
        {
            throw new NotImplementedException();
        }

        private int GetHighestCredit()
        {
            throw new NotImplementedException();
        }

        private string GetInterestTypeIndicator()
        {
            throw new NotImplementedException();
        }

        private int GetOriginalChargeOffAmount()
        {
            throw new NotImplementedException();
        }

        private string GetPortfolioType()
        {
            throw new NotImplementedException();
        }

        private string GetSpecialComment()
        {
            throw new NotImplementedException();
        }

        private int GetTermsDuration()
        {
            throw new NotImplementedException();
        }

        private string GetTermsFrequency()
        {
            throw new NotImplementedException();
        }

        private string GetTransactionType()
        {
            throw new NotImplementedException();
        }
    }
}

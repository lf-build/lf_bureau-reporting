﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Abstractions
{
    public interface ILookups
    {
        Dictionary<string, string> AccountStatusCodes { get; }
        Dictionary<string, string> AccountTypes { get; }
        Dictionary<string, Tuple<string, bool, bool, bool, bool, bool>> AccountTypePortfolioMap { get; }
        Dictionary<string, Tuple<string, bool, bool, bool, bool, bool>> SpecialCommentPortfolioMap { get; }
        Dictionary<string, string> ComplianceConditionCodes { get; }
        Dictionary<string, string> ConsumerInformationIndicators { get; }
        Dictionary<string, string> EcoaCodes { get; }
        Dictionary<string, string> GenerationCodes { get; }
        Dictionary<string, string> InterestTypeIndicators { get; }
        Dictionary<string, string> PaymentRatings { get; }
        Dictionary<string, string> PortfolioTypes { get; }
        Dictionary<string, string> ResidenceCodes { get; }
        Dictionary<string, string> TermsFrequencyCodes { get; }
        Dictionary<string, string> TransactionTypes { get; }
        Dictionary<string, string> CreditorClassificationCodes { get; }
        Dictionary<string, string> AgencyIdentifiers { get; }
        Dictionary<string, string> SpecializedPaymentIndicators { get; }
        Dictionary<string, string> ChangeIndicators { get; }
        Dictionary<string, string> States { get; }
        Dictionary<string, string> Countries { get; }
        Dictionary<string, string> PaymentStatuses { get; }
        Dictionary<string, string> AddressIndicators { get; }
    }
}


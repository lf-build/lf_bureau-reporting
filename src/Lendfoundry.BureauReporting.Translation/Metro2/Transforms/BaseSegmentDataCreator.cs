﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using LendFoundry.BureauReporting.Core.Domain.Borrower;
using LendFoundry.BureauReporting.Core.Domain.Lookups;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Transforms
{
    public class BaseSegmentDataCreator : ISegmentDataCreator
    {
        private ReportData ReportData;
        private BorrowerInfo PrimaryBorrower;
        private ILookups Lookups;

        public BaseSegmentDataCreator(ReportData reportData)
        {
            ReportData = reportData;

            // TODO: Think about moving this to the BorrowerInfo object as a method/property
            PrimaryBorrower = ReportData.BorrowerInfo.FirstOrDefault(b => b.Role == "Primary");

            Lookups = new Lookups(); // TODO: Inject lookups?
        }

        public Segment GetSegment()
        {
            var segment = new BaseSegment();

            // Account Number 
            segment.ConsumerAccountNumber = ReportData.LoanInfo.LoanNumber;

            // Account Status 
            segment.AccountStatus = GetAccountStatus();

            // Actual payment amount
            segment.ActualPaymentAmount = ReportData.AccountInfo.ActualPaymentAmount;

            // Account Type 
            segment.AccountType = GetAccountType();

            // Address Indicator
            segment.AddressIndicator = GetAddressIndicator();

            // First line of address
            segment.FirstLineOfAddress = PrimaryBorrower.Address.Line1;

            // Second line of address
            segment.SecondLineOfAddress = PrimaryBorrower.Address.Line2;

            // City
            segment.City = PrimaryBorrower.Address.City;

            // Country code
            segment.State = PrimaryBorrower.Address.CountryCode;

            // Credit Limit 
            segment.CreditLimit = ReportData.AccountInfo.CreditLimit;

            // Amount Past Due
            segment.AmountPastDue = CalculateAmountPastDue();

            // Compliance condition code
            segment.ComplianceConditionCode = GetComplianceConditionCode();

            // Consumer Indicator 
            segment.ConsumerInformationIndicator = GetConsumerInformationIndicator();

            // Current Balance
            segment.CurrentBalance = GetCurrentBalance();

            // Date Closed 
            segment.DateClosed = GetAccountClosureDate();

            // Date of Account Information 
            segment.DateofAccountInformation = ReportData.AccountInfo.DateOfAccountInformation;

            // Date of Birth 
            segment.DateOfBirth = PrimaryBorrower.Borrower.DateOfBirth;

            // Date of First Delinquency 
            segment.DateofFirstDelinquency = GetDateOfFirstDelinquency();

            // Date of Last Payment 
            segment.DateofLastPayment = GetDateOfLastPayment();

            // Date Opened
            segment.DateOpened = ReportData.LoanInfo.LoanFundedDate;

            // ECOA Code 
            segment.ECOACode = GetEcoaCode();

            // First Name 
            segment.FirstName = PrimaryBorrower.Borrower.FirstName;

            // Generation Code
            segment.GenerationCode = GetGenerationCode();

            // Highest Credit 
            segment.HighestCredit = CalculateHighestCredit();

            // Interest Type Indicator 
            segment.InterestTypeIndicator = GetInterestTypeIndicator();

            // Last Name - Surname
            segment.LastName = PrimaryBorrower.Borrower.LastName;

            // Middle Name 
            segment.MiddleName = PrimaryBorrower.Borrower.MiddleName;

            // Original Charge Off Amount
            segment.OriginalChargeoffAmount = GetOriginalChargeOffAmount();

            // Payment History 
            segment.PaymentHistory = GetPaymentHistory();

            // Payment Rating 
            segment.PaymentRating = GetPaymentRating();

            // Portfolio Type 
            segment.PortfolioType = ReportData.LoanInfo.Product.Portfolio;

            // Residence Code 
            segment.ResidenceCode = PrimaryBorrower.Address.Own ? "O" : "R";

            // Scheduled Monthly Payment
            segment.ScheduledMonthlyPaymentAmount = ReportData.LoanInfo.MonthlyPaymentAmount;

            // Social Security Number - SSN 
            segment.SocialSecurityNumber = PrimaryBorrower.Borrower.SocialSecurityNumber;

            // Special Comment 
            segment.SpecialComment = GetSpecialComment();

            // State(Province)
            segment.State = PrimaryBorrower.Address.State;

            // Telephone Number
            segment.TelephoneNumber = PrimaryBorrower.Address.Telephone;

            // Terms Duration
            segment.TermsDuration = ReportData.LoanInfo.Terms;

            // Terms Frequency 
            segment.TermsFrequency = GetTermsFrequency();

            // Transaction Type
            segment.ConsumerTransactionType = GetTransactionType();

            // Zip Code - Postal Code
            segment.ZipCode = PrimaryBorrower.Address.ZipCode;

            return segment;
        }

        private double? GetCurrentBalance()
        {
            // calculate the current balance
            // current balance = amount funded - amount pending

            // get amount pending from the payment history
            var charges = ReportData.PaymentHistory.Sum(payment => payment.PaymentAmount.Charges);
            var interest = ReportData.PaymentHistory.Sum(payment => payment.PaymentAmount.Interest);
            var principal = ReportData.PaymentHistory.Sum(payment => payment.PaymentAmount.Principal);

            var totalAmountPaid = principal + interest + charges;


            // TODO: Verify that this is the correct method. What if history is not available but customer
            // has already paid?
            var balance = ReportData.LoanInfo.FundedLoanAmount - totalAmountPaid;

            return balance;

        }

        private DateTime? GetDateOfFirstDelinquency()
        {
            return null; // TODO: Calculate date of first delinquency (if borrower is delinquent)
        }

        private string GetAccountType()
        {
            return "7B"; // TODO: Compute the account type code
        }

        private string GetAddressIndicator()
        {
            return string.Empty; // TODO: Get the address indicator
        }

        private string GetAccountStatus()
        {
            return "11"; // TODO: Compute the other status codes
        }

        private string GetTransactionType()
        {
            return string.Empty; // TODO: Compute the transaction type code for all other scenarios
        }

        private string GetTermsFrequency()
        {
            return ReportData.LoanInfo.Product.Frequency; // TODO: In case of deferred payments, "D" should be returned
        }

        private string GetSpecialComment()
        {
            return string.Empty; // TODO: Compute the special comment code
        }

        private string GetPaymentRating()
        {
            return "0"; // TODO: zero is for current accounts, implement this for other statuses
        }

        private string GetPaymentHistory()
        {
            return "BBBBBBBBBBBBBBBBBBBBBBBB";  // TODO: Implement payment history generation
        }

        private int? GetOriginalChargeOffAmount()
        {
            return 0; // TODO: Implement charge off scenario
        }

        private string GetInterestTypeIndicator()
        {
            return Lookups.InterestTypeIndicators[ReportData.AccountInfo.InterestType];
        }

        private int CalculateHighestCredit()
        {
            return 0; // TODO: Implment highest credit calculation
        }

        private string GetGenerationCode()
        {
            var generation = PrimaryBorrower.Borrower.Generation;

            if (!string.IsNullOrWhiteSpace(generation))
            {
                // TODO: If generation code not found, blank fill (but report it to the tenant later)
                return Lookups.GenerationCodes[PrimaryBorrower.Borrower.Generation];
            }

            return null;
        }

        private string GetEcoaCode()
        {
            return "1"; // ECOA Code for primary borrower is 1, but this must cover deceased, terminated etc. also.

            // TODO: ECOA codes for Deceased, Terminated etc. also must be returned
        }

        private DateTime? GetDateOfLastPayment()
        {
            return null; // TODO: Implement this
            // throw new NotImplementedException();
        }

        private DateTime? GetAccountClosureDate()
        {
            return null; // TODO: Extract closure date
        }

        private string GetConsumerInformationIndicator()
        {
            return string.Empty; // TODO: Compute the code
        }

        private string GetComplianceConditionCode()
        {
            return string.Empty; // TODO: Compute the code
        }

        private double CalculateAmountPastDue()
        {
            return 0; // TODO: Implement the calculation for this
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Payments
{
    public class Amount
    {
        public int Principal { get; set; }
        public int Interest { get; set; }
        public int Fees { get; set; }
        public int Charges { get; set; }
    }
}
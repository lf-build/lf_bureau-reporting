﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Attributes;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    [Validator(typeof(HeaderRecordSegmentValidator))]
    public class HeaderRecordSegment : FileSegment
    {
        public string BlockDescriptorWord { get; set; }

        public string RecordDescriptorWord { get; set; }

        public string RecordIdentifier { get; set; }

        public int CycleNumber { get; set; }

        public string InnovisProgramIdentifier { get; set; }

        public string EquifaxProgramIdentifier { get; set; }

        public string ExperianProgramIdentifier { get; set; }

        public string TransUnionProgramIdentifier { get; set; }

        public DateTime? ActivityDate { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? ProgramDate { get; set; }
        public DateTime? ProgramRevisionDate { get; set; }

        public string ReporterName { get; set; }

        public string ReporterAddress { get; set; }
        public string ReporterTelephoneNumber { get; set; }

        public string SoftwareVendorName { get; set; }

        public string SoftwareVersionNumber { get; set; }
    }
}
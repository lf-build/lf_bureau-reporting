﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class UpdatedConsumerInfoValidator : ViewModelValidator<UpdatedConsumerInfo>
    {
        private IValidationContext Context { get; set; }
        public UpdatedConsumerInfoValidator(UpdatedConsumerInfo updatedConsumerInfo, IValidationContext context) : base(updatedConsumerInfo, context)
        {
            Context = context;

            ValidateNewIdentificationNumber();
            ValidateNewConsumerAccountNumber();
            RuleFor(data => data.ChangeIndicator)
                                .Must(BeValidChangeIndicator)
                                .WithMessage("The change indicator contains an unrecognized value.");
        }

        private void ValidateNewIdentificationNumber()
        {
            RuleFor(data => data.NewIdentificationNumber)
                                .Matches("^[a-zA-Z0-9]*$")
                                .WithMessage("NewIdentificationNumber must contain only numbers and letters.");
        }

        private bool BeValidChangeIndicator(string arg)
        {
            return Context.Lookups.ChangeIndicators.ContainsKey(arg);
        }

        private void ValidateNewConsumerAccountNumber()
        {
            RuleFor(data => data.NewConsumerAccountNumber)
                                .NotEmpty()
                                    .WithMessage("AccountNumber is required.")
                                .Length(1, 30)
                                    .WithMessage("AccountNumber must be between 1 and 30 characters long.")
                                .Must(NotContainOnlyZeroes)
                                    .WithMessage("AccountNumber must not contain only zeroes.")
                                .Must(NotContainEmbeddedSpaces)
                                    .WithMessage("AccountNumber must not contain spaces.")
                                .Matches("^[a-zA-Z0-9]*$")
                                    .WithMessage("AccountNumber must contain only numbers and letters.");
        }

        private bool NotContainOnlyZeroes(string arg)
        {
            return !(arg.Count(c => c == '0') == arg.Length);
        }

        private bool NotContainEmbeddedSpaces(string arg)
        {
            return !arg.Contains(" ");
        }
    }
}

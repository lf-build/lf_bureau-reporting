﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class L1Segment : FileSegment
    {
        public string SegmentIdentifier { get; set; }

        public string ChangeIndicator { get; set; }

        public string NewConsumerAccountNumber { get; set; }

        public string NewIdentificationNumber { get; set; }

        public string Reserved { get; set; }

    }
}
﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Abstractions
{
    public interface IValidationContext
    {
        ITenantTime TenantTime { get; set; }
        ILookups Lookups { get; set; }
        string AccountNumber { get; set; }
        DateTime AccountOpeningDate { get; set; }
        string AccountTypeName { get; set; }
        string AccountTypeCode { get; set; }
        string AccountStatusName { get; set; }
        string AccountStatusCode { get; set; }
        string TermsFrequency { get; set; }
        string Portfolio { get; set; }
    }
}

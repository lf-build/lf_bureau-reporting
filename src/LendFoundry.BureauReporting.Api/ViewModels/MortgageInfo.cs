﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes part of K3 segment info of Metro2, but is exposed separately. The other details are in SecondaryMarketigAgencyInfo.
    /// </summary>
    public class MortgageInfo
    {
        public long MortgageIdentificationNumber { get; set; }
    }
}

// Mortgage_IdentificationNumber
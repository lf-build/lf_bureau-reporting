﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// Constitutes the K2 segment of Metro2
    /// </summary>
    public class AccountPurchaseSaleInfo
    {
        public string PortfolioIndicator { get; set; }
        public string PurchasedPortfolioOrSoldToName { get; set; }
    }
}

//AccountPurchaseSale_PortfolioIndicator
//AccountPurchaseSale_PurchasedPortfolioOrSoldToName
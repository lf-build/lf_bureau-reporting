﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class N1Segment : FileSegment
    {
        public string SegmentIdentifier { get; set; }

        public string EmployerName { get; set; }

        public string FirstLineOfEmployerAddress { get; set; }

        public string SecondLineOfEmployerAddress { get; set; }

        public string EmployerCity { get; set; }

        public string EmployerState { get; set; }

        public string EmployerZipCode { get; set; }

        public string Occupation { get; set; }

        public string Reserved { get; set; }

    }
}





﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class ReportDataValidator : AbstractValidator<ReportData>
    {
        private ReportData ReportData { get; set; }
        public ReportDataValidator(ReportData reportData, ILookups lookups, IValidationContext context)
        {
            ReportData = reportData;

            // first check to ensure that no required constituent part of the incoming data is null
            RuleFor(data => data.AccountInfo)
                                .NotNull()
                                .WithMessage("AccountInfo is required.");

            RuleFor(data => data.BorrowerInfo)
                                .NotNull()
                                .WithMessage("BorrowerInfo is required.");

            RuleFor(data => data.BorrowerEmploymentInfo).NotNull()
                                .NotNull()
                                .WithMessage("BorrowerEmploymentInfo is required.");

            RuleFor(data => data.AccountPurchaseAndSaleInfo)
                                .NotNull()
                                .WithMessage("AccountPurchaseAndSaleInfo is required.");

            RuleFor(data => data.CoSigners)
                                .NotNull()
                                .WithMessage("Cosigner info is required.");

            RuleFor(data => data.CreditorInfo)
                                .NotNull()
                                .WithMessage("CreditorInfo is required.");

            RuleFor(data => data.MortgageInfo)
                                .NotNull()
                                .WithMessage("MortgageInfo is required.");

            RuleFor(data => data.SpecializedPaymentInfo)
                                .NotNull()
                                .WithMessage("SpecializedPaymentInfo is required.");

            RuleFor(data => data.UpdatedConsumerInfo)
                                .NotNull()
                                .WithMessage("UpatedConsumerInfo is required.");

            // assign validators to the parts that constitute the input data
            RuleFor(data => data.AccountInfo).SetValidator(new AccountInfoValidator(ReportData.AccountInfo, context));
            RuleFor(data => data.BorrowerInfo).SetValidator(new BorrowerInfoValidator(ReportData.BorrowerInfo, context));
            RuleFor(data => data.BorrowerEmploymentInfo).SetValidator(new BorrowerEmploymentInfoValidator(ReportData.BorrowerEmploymentInfo, context));
            RuleFor(data => data.AccountPurchaseAndSaleInfo).SetValidator(new AccountPurchaseSaleInfoValidator(ReportData.AccountPurchaseAndSaleInfo, context));

            // setting the validator for each member of the collection in a loop instead of directly assigning it to the collection because of the
            // constructur signature of the validator.
            foreach (var cosignerInfo in ReportData.CoSigners)
            {
                RuleFor(data => cosignerInfo).SetValidator(new CoSignerInfoValidator(cosignerInfo, context));
            }

            RuleFor(data => data.CreditorInfo).SetValidator(new CreditorInfoValidator(ReportData.CreditorInfo, context));
            RuleFor(data => data.MortgageInfo).SetValidator(new MortgageInfoValidator(ReportData.MortgageInfo, context));
            RuleFor(data => data.SpecializedPaymentInfo).SetValidator(new SpecializedPaymentInfoValidator(ReportData.SpecializedPaymentInfo, context));
            RuleFor(data => data.UpdatedConsumerInfo).SetValidator(new UpdatedConsumerInfoValidator(ReportData.UpdatedConsumerInfo, context));
        }
    }
}

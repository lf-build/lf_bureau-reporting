﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Account
{
    public class ClosureStatusModel : AccountStatusModel
    {
        public string Status { get; set; }
        public DateTime ClosureDate { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}

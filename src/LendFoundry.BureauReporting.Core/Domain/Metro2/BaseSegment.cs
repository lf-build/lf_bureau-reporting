﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Metro2
{
    public class BaseSegment : Segment
    {
        public string BlockDescriptorWord { get; set; }

        public string RecordDescriptorWord { get; set; }

        public string ProcessingIndicator { get; set; }

        public string TimeStamp { get; set; }

        public string CorrectionIndicator { get; set; }

        public string IdentificationNumber { get; set; }

        public string CycleIdentifier { get; set; }

        public string ConsumerAccountNumber { get; set; }

        public string PortfolioType { get; set; }

        public string AccountType { get; set; }

        public DateTime? DateOpened { get; set; }

        public double? CreditLimit { get; set; }

        public int HighestCredit { get; set; }

        public string InterestTypeIndicator { get; set; }

        public int? TermsDuration { get; set; }

        public string TermsFrequency { get; set; }

        public double? ScheduledMonthlyPaymentAmount { get; set; }

        public double? ActualPaymentAmount { get; set; }

        public string AccountStatus { get; set; }

        public string PaymentRating { get; set; }

        public string PaymentHistory { get; set; }

        public string SpecialComment { get; set; }

        public string ComplianceConditionCode { get; set; }

        public double? CurrentBalance { get; set; }

        public double AmountPastDue { get; set; }

        public int? OriginalChargeoffAmount { get; set; }

        public DateTime? BillingDate { get; set; }
               
        public DateTime DateofAccountInformation { get; set; }
        public DateTime? DateofFirstDelinquency { get; set; }
        public DateTime? DateClosed { get; set; }
        public string Reserved { get; set; }
        public DateTime? DateofLastPayment { get; set; }
        public string ConsumerTransactionType { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string GenerationCode { get; set; }
        public string SocialSecurityNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string TelephoneNumber { get; set; }
        public string ECOACode { get; set; }
        public string ConsumerInformationIndicator { get; set; }
        public string CountryCode { get; set; }
        public string FirstLineOfAddress { get; set; }
        public string SecondLineOfAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string AddressIndicator { get; set; }
        public string ResidenceCode { get; set; }

        public string TransactionType { get; set; }

        public string Accept(IMetro2Visitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}

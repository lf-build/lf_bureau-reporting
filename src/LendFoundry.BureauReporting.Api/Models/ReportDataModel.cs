﻿using LendFoundry.BureauReporting.Api.Models.Account;
using LendFoundry.BureauReporting.Api.Models.Borrower;
using LendFoundry.BureauReporting.Api.Models.Loan;
using LendFoundry.BureauReporting.Api.Models.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models
{
    public class ReportDataModel
    {
        public AccountInfoModel AccountInfo { get; set; }
        public ICollection<BorrowerInfoModel> BorrowerInfo { get; set; }
        public LoanInfoModel LoanInfo { get; set; }
        //public ProductInfoModel ProductInfo { get; set; }
        public ICollection<PaymentRecordModel> PaymentHistory { get; set; }
    }
}

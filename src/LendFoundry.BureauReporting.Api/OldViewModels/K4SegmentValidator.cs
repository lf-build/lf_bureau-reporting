﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K4SegmentValidator : AbstractValidator<K4Segment>
    {
        private const string SegmentName = "K4 Segment";
        public K4SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmentIdentifier cannot be empty.");
            RuleFor(segment => segment.SpecializedPaymentIndicator).NotEmpty().WithMessage($"{SegmentName} - SpecializedPaymentIndicator cannot be empty.");
            RuleFor(segment => segment.DeferredPaymentStartDate).NotEmpty().WithMessage($"{SegmentName} - DeferredPaymentStartDate cannot be empty.");
            RuleFor(segment => segment.PaymentDueDate).NotEmpty().WithMessage($"{SegmentName} - PaymentDueDate cannot be empty.");
            RuleFor(segment => segment.PaymentAmount).NotEmpty().WithMessage($"{SegmentName} - PaymentAmount cannot be empty.");
        }
    }
}

﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class N1SegmentValidator : AbstractValidator<N1Segment>
    {
        private const string SegmentName = "N1 Segment";
        public N1SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmentIdentifier cannot be empty.");
            RuleFor(segment => segment.EmployerName).NotEmpty().WithMessage($"{SegmentName} - EmployerName cannot be empty.");
            RuleFor(segment => segment.Occupation).NotEmpty().WithMessage($"{SegmentName} - Occupation cannot be empty.");
        }
    }
}

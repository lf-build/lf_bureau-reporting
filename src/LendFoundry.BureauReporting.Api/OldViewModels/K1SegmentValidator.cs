﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K1SegmentValidator : AbstractValidator<K1Segment>
    {
        private const string SegmentName = "K1 Segment";
        public K1SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmentIdentifier cannot be empty.");
            RuleFor(segment => segment.OriginalCreditorName).NotEmpty().WithMessage($"{SegmentName} - OriginalCreditorName cannot be empty.");
        }
    }
}

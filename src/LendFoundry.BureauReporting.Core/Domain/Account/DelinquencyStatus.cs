﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class DelinquencyStatus : AccountStatus
    {
        public int NumberOfDaysDelinquent { get; set; }
        public int DelinquentAmount { get; set; }
        public DateTime LastDelinquentDate { get; set; }
    }
}

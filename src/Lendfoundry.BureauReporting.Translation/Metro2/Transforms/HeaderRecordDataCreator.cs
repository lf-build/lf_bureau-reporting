﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System.Globalization;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Transforms
{
    public class HeaderRecordDataCreator : ISegmentDataCreator
    {
        private ReportData ReportData { get; }
        public HeaderRecordDataCreator(ReportData reportData)
        {
            ReportData = reportData;
        }

        public Segment GetSegment()
        {
            var header = new HeaderRecordSegment();
            header.CycleNumber = null; // TODO: Get cycle number if applicable
            header.InnovisProgramIdentifier = "INNOVIS"; // TODO: Get the identifier
            header.EquifaxProgramIdentifier = "EQUIFAX"; // TODO: Get the identifier
            header.ExperianProgramIdentifier = "EXPERIAN"; // TODO: Get unique identification number assigned by this consumer reporting agency
            header.TransUnionProgramIdentifier = "TRANSUNIION"; // TODO: Get unique identification number assigned by this consumer reporting agency
            header.ActivityDate = DateTime.UtcNow; // DateTime.ParseExact(DateTime.Now.ToString(), "MMDDYYYY", CultureInfo.InvariantCulture);
            header.DateCreated = DateTime.UtcNow; // DateTime.ParseExact(DateTime.Now.ToString(), "MMDDYYYY", CultureInfo.InvariantCulture);
            header.ProgramDate = DateTime.UtcNow; // DateTime.ParseExact(DateTime.Now.ToString(), "MMDDYYYY", CultureInfo.InvariantCulture);
            header.ProgramRevisionDate = DateTime.UtcNow; //DateTime.ParseExact(DateTime.Now.ToString(), "MMDDYYYY", CultureInfo.InvariantCulture);
            header.ReporterName = "SIGMA INFOSOLUTIONS";
            header.ReporterAddress = "17310 RED HILL AVE, 340, IRVINE, CA, 92614";
            header.ReporterTelephoneNumber = "1-888-861-7360";
            header.SoftwareVendorName = "SIGMA INFOSOLUTIONS";
            header.SoftwareVersionNumber = "1.0";
            return header;
        }
    }
}

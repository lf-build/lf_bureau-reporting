﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Account
{
    public class DelinquencyStatusModel : AccountStatusModel
    {
        public int NumberOfDaysDelinquent { get; set; }
        public int DelinquentAmount { get; set; }
        public DateTime LastDelinquentDate { get; set; }
    }
}

﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class L1SegmentValidator : AbstractValidator<L1Segment>
    {
        private const string SegmentName = "L1 Segment";
        public L1SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmentIdentifier cannot be empty.");
            RuleFor(segment => segment.ChangeIndicator).NotEmpty().WithMessage($"{SegmentName} - ChangeIndicator cannot be empty.");
            RuleFor(segment => segment.NewConsumerAccountNumber).NotEmpty().WithMessage($"{SegmentName} - NewConsumerAccountNumber cannot be empty.");
            RuleFor(segment => segment.NewIdentificationNumber).NotEmpty().WithMessage($"{SegmentName} - NewIdentificationNumber cannot be empty.");
        }
    }
}

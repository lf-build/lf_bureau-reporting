﻿using LendFoundry.BureauReporting.Core.Abstractions;
using LendFoundry.BureauReporting.Core.Domain.Lookups;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Unpacked
{
    public class BaseSegmentFormatter
    {
        private Metro2Data Metro2Data { get; }

        public BaseSegmentFormatter(Metro2Data metro2Data)
        {
            Metro2Data = metro2Data;
        }

        public string GenerateSegment()
        {
            StringBuilder sb = new StringBuilder(426);

            // 1 - 4 ----------- Record Descriptor Word(RDW)
            sb.Append(GetRecordDescriptor());

            // 5 --------------- Processing Indicator
            sb.Append("1"); // 1 for base segment

            // 6 - 19 ---------- Time Stamp (Date and time of actual account information update)
            sb.Append(DateTime.UtcNow.ToUniversalTime().ToString()); // TODO: Should this be in UTC?

            // 20 -------------- Correction Indicator (0 - Normal update, 1 - correction update)
            sb.Append("0"); // TODO: Implement correction update

            // 21 - 40 --------- Identification Number
            sb.Append(GetIdentificationNumber());

            // 41 - 42 --------- Cycle Identifier
            sb.Append("  "); // TODO: Field is required if reporting by cycles, otherwise blank fill.

            // 43 - 72 --------- Consumer Account Number
            sb.Append(Metro2Data.BaseSegment.ConsumerAccountNumber);

            // 73 -------------- Portfolio Type
            sb.Append(Metro2Data.BaseSegment.PortfolioType);

            // 74 - 75 --------- Account Type
            sb.Append(Metro2Data.BaseSegment.AccountType);

            // 76 - 83 --------- Date Opened
            sb.Append(Metro2Data.BaseSegment.DateOpened);

            // 84 - 92 --------- Credit Limit
            sb.Append(Metro2Data.BaseSegment.CreditLimit);

            // 93 - 101 -------- Highest Credit or Original Loan Amount
            sb.Append(Metro2Data.BaseSegment.HighestCredit);

            // 102 - 104 ------- Terms Duration
            sb.Append(Metro2Data.BaseSegment.TermsDuration);

            // 105 ------------- Terms Frequency
            sb.Append(Metro2Data.BaseSegment.TermsFrequency);

            // 106 - 114 ------- Scheduled Monthly Payment Amount
            sb.Append(Metro2Data.BaseSegment.ScheduledMonthlyPaymentAmount);

            // 115 - 123 ------- Actual Payment Amount
            sb.Append(Metro2Data.BaseSegment.ActualPaymentAmount);

            // 124 - 125 ------- Account Status
            sb.Append(Metro2Data.BaseSegment.AccountStatus);

            // 126 ------------- Payment Rating
            sb.Append(Metro2Data.BaseSegment.PaymentRating);

            // 127 - 150 ------- Payment History Profile
            sb.Append(Metro2Data.BaseSegment.PaymentHistory);

            // 151 - 152 ------- Special Comment
            sb.Append(Metro2Data.BaseSegment.SpecialComment);

            // 153 - 154 ------- Compliance Condition Code
            sb.Append(Metro2Data.BaseSegment.ComplianceConditionCode);

            // 155 - 163 ------- Current Balance
            sb.Append(Metro2Data.BaseSegment.CurrentBalance);

            // 164 - 172 ------- Amount Past Due
            sb.Append(Metro2Data.BaseSegment.AmountPastDue);

            // 173 - 181 ------- Original Charge-off Amount
            sb.Append(Metro2Data.BaseSegment.OriginalChargeoffAmount);

            // 182 - 189 ------- Billing Date
            sb.Append(Metro2Data.BaseSegment.BillingDate);

            // 190 - 197 ------- FCRA Compliance/ Date of First Delinquency
            sb.Append(Metro2Data.BaseSegment.DateofFirstDelinquency);

            // 198 - 205 ------- Date Closed
            sb.Append(Metro2Data.BaseSegment.DateClosed);

            // 206 - 213 ------- Date of Last Payment
            sb.Append(Metro2Data.BaseSegment.DateofLastPayment);

            // 214 - 230 ------- Reserved
            sb.Append(" ".PadLeft(17)); // 17 characters blank filled

            // 231 ------------- Consumer Transaction Type
            sb.Append(Metro2Data.BaseSegment.ConsumerTransactionType);

            // 232 - 256 ------- Surname
            sb.Append(Metro2Data.BaseSegment.LastName);

            // 257 - 276 ------- First Name
            sb.Append(Metro2Data.BaseSegment.FirstName);

            // 277 - 296 ------- Middle Name
            sb.Append(Metro2Data.BaseSegment.MiddleName);

            // 297 ------------- Generation Code
            sb.Append(Metro2Data.BaseSegment.GenerationCode);

            // 298 - 306 ------- Social Security Number
            sb.Append(Metro2Data.BaseSegment.SocialSecurityNumber);

            // 307 - 314 ------- Date of Birth
            sb.Append(Metro2Data.BaseSegment.DateOfBirth);

            // 315 - 324 ------- Telephone Number
            sb.Append(Metro2Data.BaseSegment.TelephoneNumber);

            // 325 ------------- ECOA Code
            sb.Append(Metro2Data.BaseSegment.ECOACode);

            // 326 - 327 ------- Consumer Information Indicator
            sb.Append(Metro2Data.BaseSegment.ConsumerInformationIndicator);

            // 328 - 329 ------- Country Code
            sb.Append(Metro2Data.BaseSegment.CountryCode);

            //330 - 361 ------- First Line of Address
            sb.Append(Metro2Data.BaseSegment.FirstLineOfAddress);

            //362 - 393 ------- Second Line of Address
            sb.Append(Metro2Data.BaseSegment.SecondLineOfAddress);

            //394 - 413 ------- City
            sb.Append(Metro2Data.BaseSegment.City);

            //414 - 415 ------- State
            sb.Append(Metro2Data.BaseSegment.State);

            //416 - 424 ------- Postal / Zip Code
            sb.Append(Metro2Data.BaseSegment.ZipCode);

            //425 ------------- Address Indicator
            sb.Append(Metro2Data.BaseSegment.AddressIndicator);

            //426 ------------- Residence Code
            sb.Append(Metro2Data.BaseSegment.ResidenceCode);

            return sb.ToString();
        }

        private string GetIdentificationNumber()
        {
            return "16641121520150741270SIGMAINFOSOLUTIONSCA"; // TODO: Get Identification Number from environment or config value
        }

        private string GetRecordDescriptor()
        {
            return "BASE"; // TODO: Calculate record descriptor value
        }
    }
}
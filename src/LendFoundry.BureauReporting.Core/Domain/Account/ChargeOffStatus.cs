﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class ChargeOffStatus : AccountStatus
    {
        public string Status { get; set; }
        public DateTime RecommendedDate { get; set; }
        public DateTime InitiatedDate { get; set; }
        public DateTime CompletionDate { get; set; }
        public int ChargeOffAmount { get; set; }
    }
}

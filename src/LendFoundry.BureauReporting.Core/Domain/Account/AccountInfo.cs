﻿using LendFoundry.BureauReporting.Core.Abstractions;
using LendFoundry.BureauReporting.Core.Domain.Account;
using LendFoundry.BureauReporting.Core.Domain.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class AccountInfo
    {
        #region Newly Added Properties
        public string AccountNumber { get; set; }
        public string InterestType { get; set; } // to be assigned to the InterestTypeIndicator in base segment
        public DateTime DateOfAccountInformation { get; set; }

        public double ActualPaymentAmount { get; set; }

        public double CreditLimit { get; set; }

        #endregion Newly Added Properties


        public AccountStatus AccountStatus { get; set; }
        public string PaymentStatus { get; set; } // payment status is different from account status
        public Amount TotalAmountDue { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Account
{
    public class SettledStatusModel : AccountStatusModel
    {
        public string Status { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public int LastPaymentAmount { get; set; }
    }
}

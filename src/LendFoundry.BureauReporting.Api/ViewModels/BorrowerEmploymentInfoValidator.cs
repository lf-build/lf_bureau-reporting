﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class BorrowerEmploymentInfoValidator : ViewModelValidator<BorrowerEmploymentInfo>
    {
        public BorrowerEmploymentInfoValidator(BorrowerEmploymentInfo borrowerEmploymentInfo, IValidationContext context) : base (borrowerEmploymentInfo, context)
        {
            // No validations as of now.
        }
    }
}

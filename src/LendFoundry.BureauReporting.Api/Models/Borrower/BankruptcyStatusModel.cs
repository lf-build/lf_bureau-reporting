﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.Models.Borrower
{
    public class BankruptcyStatusModel : BorrowerStatusModel
    {
        public string Type { get; set; }
        public string Status { get; set; }
        public DateTime PetitionDate { get; set; }
        public DateTime DischargeDate { get; set; }
        public DateTime WithdrawnDate { get; set; }
    }
}

﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class J2SegmentValidator : AbstractValidator<J2Segment>
    {
        private const string SegmentName = "J2 Segment";
        public J2SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmemtIdentifier cannot be empty.");

            RuleFor(segment => segment.Surname).NotEmpty().WithMessage($"{SegmentName} - Surname cannot be empty.");

            RuleFor(segment => segment.FirstName).NotEmpty().WithMessage($"{SegmentName} - FirstName cannot be empty.");

            RuleFor(segment => segment.MiddleName).NotEmpty().WithMessage($"{SegmentName} - MiddleName cannot be empty.");

            RuleFor(segment => segment.GenerationCode).NotEmpty().WithMessage($"{SegmentName} - GenerationCode cannot be empty.");

            RuleFor(segment => segment.SocialSecurityNumber).NotEmpty().WithMessage($"{SegmentName} - SocialSecurityNumber cannot be empty.");

            RuleFor(segment => segment.DateOfBirth).NotNull().WithMessage($"{SegmentName} - DateOfBirth cannot be empty.");

            RuleFor(segment => segment.ECOACode).NotEmpty().WithMessage($"{SegmentName} - ECOACode cannot be empty.");

            RuleFor(segment => segment.ConsumerInformationIndicator).NotEmpty().WithMessage($"{SegmentName} - ConsumerInformationIndicator cannot be empty.");

            RuleFor(segment => segment.FirstLineOfAddress).NotEmpty().WithMessage($"{SegmentName} - FirstLineOfAddress cannot be empty.");

            RuleFor(segment => segment.SecondLineOfAddress).NotEmpty().WithMessage($"{SegmentName} - SecondLineOfAddress cannot be empty.");

            RuleFor(segment => segment.City).NotEmpty().WithMessage($"{SegmentName} - City cannot be empty.");

            RuleFor(segment => segment.State).NotEmpty().WithMessage($"{SegmentName} - State cannot be empty.");

            RuleFor(segment => segment.ZipCode).NotEmpty().WithMessage($"{SegmentName} - ZipCode cannot be empty.");
        }
    }
}

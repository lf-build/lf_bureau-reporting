﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{ 
    public class Metro2
    {
        public BaseSegment BaseSegment { get; set; }
        public HeaderRecordSegment HeaderRecordSegment { get; set; }
        public J1Segment J1Segment { get; set; }
        public J2Segment J2Segment { get; set; }
        public K1Segment K1Segment { get; set; }
        public K2Segment K2Segment { get; set; }
        public K3Segment K3Segment { get; set; }
        public K4Segment K4Segment { get; set; }
        public L1Segment L1Segment { get; set; }
        public N1Segment N1Segment { get; set; }
        public TrailerRecordSegment TrailerRecorSegment { get; set; }
    }


    //public class ReportData
    //{
    //    // BorrowerData (Base segment)
    //    // CoSignerData (J1, J2 segments)
    //    // CreditorData (K1 segment)
    //    // AccountPurchasedFromOrSoldTo (K2 Segment)
    //    // MarketingAgencyAndMortgageData (K3 Segment)
    //    // SpecializedPaymentData (K4 Segment)
    //    // UpdatedAccountNumber (L1 Segment)
    //    // BorrowerEmploymentData (N1 Segment)
    //}
}

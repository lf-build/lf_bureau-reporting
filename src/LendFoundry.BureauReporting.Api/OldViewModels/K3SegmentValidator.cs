﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K3SegmentValidator : AbstractValidator<K3Segment>
    {
        private const string SegmentName = "K3 Segment";
        public K3SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmentIdentifier cannot be empty.");
            RuleFor(segment => segment.AgencyIdentifier).NotEmpty().WithMessage($"{SegmentName} - AgencyIdentifier cannot be empty.");
            RuleFor(segment => segment.AccountNumber).NotEmpty().WithMessage($"{SegmentName} - AccountNumber cannot be empty.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Metro2;

namespace Lendfoundry.BureauReporting.Translation.Metro2.Transforms
{
    public class TrailerRecordSegmentDataCreator : ISegmentDataCreator
    {
        private ReportData ReportData { get; }
        public TrailerRecordSegmentDataCreator(ReportData reportData)
        {
            ReportData = reportData;
        }

        public Segment GetSegment()
        {
            // TODO: Implement this
            return new TrailerRecordSegment();
        }
    }
}

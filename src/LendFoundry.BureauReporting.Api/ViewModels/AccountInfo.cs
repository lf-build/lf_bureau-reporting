﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    /// <summary>
    /// This is a part of the base segment of Metro2, separated into its own class for modularity.
    /// </summary>
    public class AccountInfo
    {
        public string AccountNumber { get; set; }
        public string AccountStatus { get; set; }
        public string AccountType { get; set; }
        public string Portfolio { get; set; }
        public double ActualPaymentAmount { get; set; }
        public double AmountPastDue { get; set; }
        public double CreditLimit { get; set; }
        public double CurrentBalance { get; set; }
        public DateTime DateOfAccountInformation { get; set; }
        public DateTime? DateOfFirstDelinquency { get; set; }
        public DateTime DateOfLastPayment { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime DateClosed { get; set; }
        public ICollection<PaymentStatus> PaymentHistory { get; set; }
    }
}




//Account_AccountNumber
//Account_AccountStatus
//Account_AccountType
//Account_Portfolio
//Account_ActualPaymentAmount
//Account_AmountPastDue
//Account_CreditLimit
//Account_CurrentBalance
//Account_DateOfAccountInformation
//Account_DateOfFirstDelinquency
//Account_DateOfLastPayment
//Account_DateOpened
//Account_DateClosed
//Account_Month1_Status
//Account_Month2_Status
//Account_Month3_Status
//Account_Month4_Status
//Account_Month5_Status
//Account_Month6_Status
//Account_Month7_Status
//Account_Month8_Status
//Account_Month9_Status
//Account_Month10_Status
//Account_Month11_Status
//Account_Month12_Status
//Account_Month13_Status
//Account_Month14_Status
//Account_Month15_Status
//Account_Month16_Status
//Account_Month17_Status
//Account_Month18_Status
//Account_Month19_Status
//Account_Month20_Status
//Account_Month21_Status
//Account_Month22_Status
//Account_Month23_Status
//Account_Month24_Status


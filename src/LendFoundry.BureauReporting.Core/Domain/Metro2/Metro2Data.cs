﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Metro2
{
    public class Metro2Data
    {
        public HeaderRecordSegment HeaderRecordSegment { get; set; }
        public BaseSegment BaseSegment { get; set; }
        public J1Segment J1Segment { get; set; }
        public J2Segment J2Segment { get; set; }
        public K1Segment K1Segment { get; set; }
        public K2Segment K2Segment { get; set; }
        public K3Segment K3Segment { get; set; }
        public K4Segment K4Segment { get; set; }
        public L1Segment L1Segment { get; set; }
        public N1Segment N1Segment { get; set; }
        public TrailerRecordSegment TrailerRecordSegment { get; set; }
    }
}

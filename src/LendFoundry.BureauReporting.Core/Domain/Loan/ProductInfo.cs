﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain
{
    public class ProductInfo
    {
        public string Frequency { get; set; }
        public string InterestRateType { get; set; }
        public string Portfolio { get; set; }
        public string ProductType { get; set; }
    }
}

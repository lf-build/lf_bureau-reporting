﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain;
using Lendfoundry.BureauReporting.Translation.Metro2.Transforms;
using Lendfoundry.BureauReporting.Translation.Metro2.Unpacked;
using System.IO;

namespace LendFoundry.BureauReporting.Services
{
    public class ReportDataService : IReportDataService
    {
        private IReportDataRepository Repository { get; }
        private IFileStore FileStore { get; }
        private ISftpHandler SftpHandler { get; }

        public ReportDataService(IReportDataRepository repository, IFileStore fileStore, ISftpHandler sftpHandler)
        {
            Repository = repository;
            FileStore = fileStore;
            SftpHandler = sftpHandler;
        }

        public void AddBureauReportData(ICollection<ReportData> reportData)
        {
            Repository.AddBureauReportData(reportData);
        }

        public void ImportFileData()
        {
            // TODO: Provide implementation for importing data from ingested files into bureau reporting repository.

            // retrieve the files to import
            FileStore.RetrieveFilesToImport();

            // transform the data from the files to the target schema


            // save the transformed data to the repository
        }

        public void SubmitReport()
        {
            var records = Repository.GetReportDataToBeSubmitted();

            // TODO: Convert to Parallel.ForEach()
            // Translate the data to Metro2
            foreach (var record in records)
            {
                // transform from target schema to metro2 schema
                var translationManager = new Metro2TranslationManager(record);
                var metro2Data = translationManager.CreateMetro2();

                // convert from metro2 schema to metro2 string
                var formattingManager = new FormattingManager(metro2Data);
                var metro2 = formattingManager.GetFormattedString();

                // Archive the metro2 file
                FileStore.SaveMetro2File(metro2);

                // Submit the Metro2 file to the bureau via SFTP
                SftpHandler.Send();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class ContactValidator : ViewModelValidator<Contact>
    {
        private Contact Contact { get; set; }
        private IValidationContext Context { get; set; }
        public ContactValidator(Contact contact, IValidationContext context) : base(contact, context)
        {
            Contact = contact;
            Context = context;

            ValidateName();
            ValidateDateOfBirth();
            ValidateSocialSecurityNumber();
            ValidateAddressFirstLine();
            ValidateAddressSecondLine();
        }

        private void ValidateName()
        {
            RuleFor(data => data.FirstName)
                .NotEmpty()
                .Matches("^[a-zA-Z-]*$")
                .WithMessage("FirstName contains invalid characters. Only letters and dashes are accepted.");

            if (!string.IsNullOrEmpty(Contact.MiddleName))
            {
                RuleFor(data => data.MiddleName)
                    .Matches("^[a-zA-Z-]*$")
                    .WithMessage("MiddleName contains invalid characters. Only letters and dashes are accepted.");
            }

            RuleFor(data => data.Surname)
                .NotEmpty()
                .Matches("^[a-zA-Z-]*$")
                .WithMessage("Surname contains invalid characters. Only letters and dashes are accepted.");

            RuleFor(data => data.Generation)
                .Must(BeValidGeneration)
                .WithMessage("Generation has an invalid value.");
        }

        private bool BeValidGeneration(string arg)
        {
            return string.IsNullOrEmpty(arg) ? true : Context.Lookups.GenerationCodes.ContainsKey(arg);
        }

        public void ValidateDateOfBirth()
        {
            //throw new NotImplementedException();
        }
        private void ValidateSocialSecurityNumber()
        {
            RuleFor(data => data.SocialSecurityNumber)
                                .NotEmpty()
                                .WithMessage("SocialSecurityNumber is required.");
        }
        private void ValidateAddressFirstLine()
        {
            // Cannot be empty and can contain A-Z Alpha, 0-9 Numeric, . (period), / (slash), - (dash) and spaces.
            RuleFor(data => data.AddressFirstLine)
                                .NotEmpty()
                                    .WithMessage("AddressFirstLine is required")
                                .Matches(@"^[a-zA-Z0-9./\s-]*$")
                                    .WithMessage("Invalid first line of address: Cannot be empty and can contain letters, numbers, spaces and the following: Period (.), Slash (/), Dash (-)");
        }
        private void ValidateAddressIndicator()
        {
            // Can be blank
            if (!string.IsNullOrEmpty(Contact.AddressIndicator))
            {
                RuleFor(data => data.AddressIndicator)
                                    .Must(BeValidAddressIndicator)
                                    .WithMessage("AddressIndicator has an invalid value.");
            }
        }

        private bool BeValidAddressIndicator(string indicator)
        {
            return Context.Lookups.AddressIndicators.ContainsKey(indicator);
        }
        private void ValidateAddressSecondLine()
        {
            RuleFor(data => data.AddressFirstLine)
                                .NotEmpty()
                                .Matches(@"^[a-zA-Z0-9./\s-]*$");
        }
        private void ValidateCountryCode()
        {
            RuleFor(data => data.CountryCode)
                .NotEmpty()
                    .WithMessage("CountryCode is required.")
                .Must(BeValidCountryCode)
                    .WithMessage($"CountryCode {Contact.CountryCode} is invalid.");
        }
        private void ValidateState()
        {
            RuleFor(data => data.State)
                                .NotEmpty()
                                    .WithMessage("State is required.")
                                .Must(BeValidState)
                                    .WithMessage($"The value specified for State ({Contact.State}) is invalid.");
        }

        private bool BeValidState(string arg)
        {
            if(Contact.CountryCode == "US")
            {
                return Context.Lookups.States.ContainsKey(arg);
            }

            return true;
        }

        private void ValidateCity()
        {
            // Truncate rightmost positions if city name is greater than 20 characters or use standard 13-character U.S. Postal Service city abbreviations. 
            if(!string.IsNullOrWhiteSpace(Contact.City))
            {
                Contact.City = Contact.City.Substring(0, 20);
            }

            RuleFor(data => data.City)
                                .NotEmpty()
                                .WithMessage("City is required.");
        }

        private void ValidateZipCode()
        {
            RuleFor(data => data.ZipCode)
                                .NotEmpty()
                                .WithMessage("ZipCode is required.");

            // TODO: Implement regex match (read documentation for more details)
        }
        private void ValidateTelephoneNumber()
        {
            RuleFor(data => data.TelephoneNumber)
                                .NotEmpty()
                                    .WithMessage("TelephoneNumber is required.")
                                .Length(10)  // length of 10 = area code + 7 digits (area codes are 3 digits, based on this: http://www.csgnetwork.com/usphoneareacodesbyac.html)
                                    .WithMessage("TelephoneNumber must have a length of 10.");

            // TODO: Implement regex match
        }
        private void ValidateResidenceCode()
        {
            RuleFor(data => data.ResidenceCode)
                .NotEmpty()
                .Must(BeValidResidenceCode)
                .WithMessage("ResidenceCode has an invalid value.");
        }

        private bool BeValidCountryCode(string arg)
        {
            return Context.Lookups.Countries.ContainsKey(arg);
        }

        private bool BeValidResidenceCode(string arg)
        {
            return Context.Lookups.ResidenceCodes.ContainsKey(arg);
        }
    }
}

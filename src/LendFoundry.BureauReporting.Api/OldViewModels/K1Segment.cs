﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class K1Segment : FileSegment
    {
        public string SegmentIdentifier { get; set; }

        public string OriginalCreditorName { get; set; }

        public string CreditorClassification { get; set; }
    }
}

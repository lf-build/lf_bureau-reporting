﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class J1SegmentValidator : AbstractValidator<J1Segment>
    {
        private const string SegmentName = "J1 Segment";
        public J1SegmentValidator()
        {
            RuleFor(segment => segment.SegmentIdentifier).NotEmpty().WithMessage($"{SegmentName} - SegmentIdentifier cannot be empty.");
            RuleFor(segment => segment.Surname).NotEmpty().WithMessage($"{SegmentName} - Surname cannot be empty.");
            RuleFor(segment => segment.FirstName).NotEmpty().WithMessage($"{SegmentName} - FirstName cannot be empty.");
            RuleFor(segment => segment.MiddleName).NotEmpty().WithMessage($"{SegmentName} - MiddleName cannot be empty.");
            RuleFor(segment => segment.GenerationCode).NotEmpty().WithMessage($"{SegmentName} - GenerationCode cannot be empty.");
            RuleFor(segment => segment.SocialSecurityNumber).NotEmpty().WithMessage($"{SegmentName} - SocialSecurityNumber cannot be empty.");
            RuleFor(segment => segment.DateOfBirth).NotNull().WithMessage($"{SegmentName} - DateOfBirth cannot be empty.");
            RuleFor(segment => segment.ECOACode).NotEmpty().WithMessage($"{SegmentName} - ECOACode cannot be empty.");
            RuleFor(segment => segment.ConsumerInformationIndicator).NotEmpty().WithMessage($"{SegmentName} - ConsumerInformationIndicator cannot be empty.");
        }
    }
}

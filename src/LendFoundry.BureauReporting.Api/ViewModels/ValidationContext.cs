﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class ValidationContext : IValidationContext
    {
        public ITenantTime TenantTime { get; set; }
        public string AccountNumber { get; set; }

        public string AccountStatusCode { get; set; }
        public DateTime AccountOpeningDate { get; set; }

        public string AccountStatusName { get; set; }

        public string AccountTypeCode { get; set; }

        public string AccountTypeName { get; set; }

        public ILookups Lookups { get; set; }
        public string TermsFrequency { get; set; }
        public string Portfolio { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using LendFoundry.BureauReporting.Core.Abstractions;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class CoSignerInfoValidator : ViewModelValidator<CoSignerInfo>
    {
        private CoSignerInfo CoSignerInfo { get; set; }
        private IValidationContext Context { get; set; }
        public CoSignerInfoValidator(CoSignerInfo cosignerInfo, IValidationContext context) : base(cosignerInfo, context)
        {
            CoSignerInfo = cosignerInfo;
            Context = context;

            RuleFor(data => data.Contact)
                                .SetValidator(new ContactValidator(cosignerInfo.Contact, context));

            RuleFor(data => data.ECOACode)
                                .NotEmpty()
                                    .WithMessage("ECOACode is required.")
                                .Must(BeValidEcoaCode)
                                    .WithMessage("ECOACode has an invalid value.");

            RuleFor(data => data.ConsumerInformationIndicator)
                                .Must(BeValidConsumerInformationIndicator)
                                .WithMessage("ConsumerInformationIndicator has an invalid value.");

            RuleFor(data => data.TransactionType)
                                .NotEmpty()
                                    .WithMessage("TransactionType is required.")
                                .Must(BeValidCosignerTransactionType)
                                    .WithMessage("TransactionType has an invalid value.");
        }

        private bool BeValidEcoaCode(string code)
        {
            if(code.ToLower() == "individual" || code.ToLower() == "maker") // these are not applicable to co-signers
            {
                return false;
            }

            return Context.Lookups.EcoaCodes.ContainsKey(code);
        }

        private bool BeValidConsumerInformationIndicator(string indicator)
        {
            return Context.Lookups.ConsumerInformationIndicators.ContainsKey(indicator);
        }

        private bool BeValidCosignerTransactionType(string transactionType)
        {
            return Context.Lookups.TransactionTypes.ContainsKey(transactionType);
        }
    }
}
﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.BureauReporting.Core.Domain.Account;
using LendFoundry.BureauReporting.Core.Domain.Borrower;
using LendFoundry.BureauReporting.Core.Domain.Loan;
using LendFoundry.BureauReporting.Core.Domain.Payments;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using LendFoundry.BureauReporting.Core.Domain;

namespace Lendfoundry.BureauReporting.Translation.Metro2
{
    public class Metro2Visitor : IReportDataVisitor
    {
        private ReportData ReportData { get; set; }
        private Metro2Data Metro2Data { get; set; }

        public Metro2Visitor(ReportData reportData)
        {
            ReportData = reportData;
            Metro2Data = new Metro2Data();
        }

        public Metro2Data GetMetro2Data()
        {
            return Metro2Data;
        }


        public void Visit(AccountInfo accountInfo)
        {
            var translator = new AccountInfoTranslator(Metro2Data);
            translator.TranslateElement(accountInfo);
        }

        public void Visit(BorrowerInfo borrowerInfo)
        {
            throw new NotImplementedException();
        }

        public void Visit(LoanInfo loanInfo)
        {
            throw new NotImplementedException();
        }

        public void Visit(PaymentInfo paymentInfo)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using Microsoft.Framework.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.EventHub.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.BureauReporting.Core.Abstractions;
using LendFoundry.BureauReporting.Core.Domain.Lookups;
using LendFoundry.BureauReporting.Services;
using Lendfoundry.BureauReporting.Repository;
using LendFoundry.BureauReporting.FileManagement;

namespace LendFoundry.BureauReporting.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            MappingEngineProvider.GetMappingEngine().CreateMappings();
        }

        // This method gets called by a runtime.
        // Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc();
            services.AddScoped<IReportDataService, ReportDataService>();
            services.AddScoped<IReportDataRepository, ReportDataRepository>();
            services.AddScoped<IFileStore, FileStore>();
            services.AddScoped<ISftpHandler, SftpHandler>();
            services.AddInstance<ILookups>(InitializeLookups());
            services.AddConfigurationService<ApplicationConfig>("10.1.1.99", 5001, "bureaureporting");
            services.AddHttpServiceLogging("bureaureporting");
            services.AddTokenHandler();
            services.AddTenantService("10.1.1.99", 5005);
            services.AddTenantTime();

            services.AddTransient<IApplicationConfig>(provider => provider.GetRequiredService<IConfigurationService<ApplicationConfig>>().Get());


            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            //services
            //    .AddEventSubscription<PaymentAdded, EventSubscriptions>()
            //    .AddEventSubscription<PaymentReceived, EventSubscriptions>()
            //    .AddEventSubscription<PaymentFailed, EventSubscriptions>();
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            //app.UseEventHub();
        }

        private ILookups InitializeLookups()
        {
            return new Lookups();
        }
    }
}

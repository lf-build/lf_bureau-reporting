﻿using LendFoundry.BureauReporting.Core.Domain;
using LendFoundry.BureauReporting.Core.Domain.Metro2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Abstractions
{
    public interface IMetro2Translator
    {
        IMetro2Translator CreateHeaderRecordSegment();
        IMetro2Translator CreateBaseSegment();
        IMetro2Translator CreateJ1Segment();
        IMetro2Translator CreateJ2Segment();
        IMetro2Translator CreateK1Segment();
        IMetro2Translator CreateK2Segment();
        IMetro2Translator CreateK3Segment();
        IMetro2Translator CreateK4Segment();
        IMetro2Translator CreateL1Segment();
        IMetro2Translator CreateN1Segment();
        IMetro2Translator CreateTrailerRecordSegment();

        Metro2Data CreateMetro2Data();
    }
}

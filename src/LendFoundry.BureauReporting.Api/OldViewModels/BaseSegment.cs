﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LendFoundry.BureauReporting.Api.ViewModels
{
    public class BaseSegment : FileSegment
    {
        public string BlockDescriptorWord { get; set; }

        public string RecordDescriptorWord { get; set; }

        public string ProcessingIndicator { get; set; }

        public string TimeStamp { get; set; }

        public string CorrectionIndicator { get; set; }

        public string IdentificationNumber { get; set; }

        public string CycleIdentifier { get; set; }

        public string ConsumerAccountNumber { get; set; }

        public string PortfolioType { get; set; }

        public string AccountType { get; set; }

        public DateTime? DateOpened { get; set; }

        public double? CreditLimit { get; set; }

        public double? HighestCredit { get; set; }

        public int? TermsDuration { get; set; }

        public int? TermsFrequency { get; set; }

        public double? ScheduledMonthlyPaymentAmount { get; set; }

        public double? ActualPaymentAmount { get; set; }

        public string AccountStatus { get; set; }

        public string PaymentRating { get; set; }

        public string PaymentHistoryProfile { get; set; }

        public string SpecialComment { get; set; }

        public string ComplianceConditionCode { get; set; }

        public double? CurrentBalance { get; set; }

        public double? AmountPastDue { get; set; }

        public double? OriginalChargeoffAmount { get; set; }

        public DateTime? BillingDate { get; set; }
        //public string DateofAccountInformation { get; set; }
        public string DateofFirstDelinquency { get; set; }
        public string DateClosed { get; set; }
        public string Reserved { get; set; }
        public string DateofLastPayment { get; set; }
        public string ConsumerTransactionType { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string GenerationCode { get; set; }
        public string SocialSecurityNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string TelephoneNo { get; set; }
        public string ECOACode { get; set; }
        public string ConsumerInformationIndicator { get; set; }
        public string CountryCode { get; set; }
        public string FirstLineOfAddress { get; set; }
        public string SecondLineOfAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string AddressIndicator { get; set; }
        public string ResidenceCode { get; set; }
        //public string DateofBankcruptcy { get; set; }
        //public double? MortgageAmount { get; set; }
    }
}

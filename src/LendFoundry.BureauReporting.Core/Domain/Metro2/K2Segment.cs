﻿using LendFoundry.BureauReporting.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Metro2
{
    public class K2Segment : Segment
    {
        public string SegmentIdentifier { get; set; }

        public string PortfolioIndicator { get; set; }

        public string PurchasedPortfolioOrSoldToName { get; set; }

        public string Reserved { get; set; }

        public string Accept(IMetro2Visitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}

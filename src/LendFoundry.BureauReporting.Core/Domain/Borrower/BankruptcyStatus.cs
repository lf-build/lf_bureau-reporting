﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Borrower
{
    public class BankruptcyStatus : BorrowerStatus
    {
        public string Type { get; set; }
        public string Status { get; set; }
        public DateTime PetitionDate { get; set; }
        public DateTime DischargeDate { get; set; }
        public DateTime WithdrawnDate { get; set; }
    }
}

﻿using LendFoundry.BureauReporting.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Core.Domain.Account
{
    public class SaleStatus : AccountStatus
    {
        public string SoldTo { get; set; }
        public DateTime SaleDate { get; set; }
    }
}
